﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Hotel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class HotelBookingControllerTestMock
    {
        #region Private Fields

        private Mock<IHotelsManager> _mockHotelsManager;
        private Mock<IUsersManager> _mockUsersManager;
        private Mock<ISecurityManager> _mockSecurityManager;
        private HotelBookingController _controller;

        private User _fakeUser;
        private List<Hotel> _fakeHotels;
        private List<HotelRooms> _fakeHotelRooms;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            _fakeUser = new User()
            {
                Id = 1,
                Login = "FakeLogin",
                FirstName = "Fake",
                Surname = "User",
                Email = "fake@gmail.com",
                Phone = "+340444444444"
            };
            _fakeHotels = new List<Hotel>()
            {
                new Hotel() {Id = 1, Name = "Fake Hotel"}
            };
            _fakeHotelRooms = new List<HotelRooms>()
            {
                new HotelRooms()
                {
                    Id =  1,
                    HotelId = 1,
                    RoomType = "Lux",
                    Price = 1000M
                }
            };

            _mockHotelsManager = new Mock<IHotelsManager>();
            var hotelsManager = _mockHotelsManager.Object;

            _mockUsersManager = new Mock<IUsersManager>();
            var usersManager = _mockUsersManager.Object;

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockHotelsManager.Setup(m => m.GetHotels()).Returns(_fakeHotels);
            _mockHotelsManager.Setup(m => m.GetHotelRoomTypes(It.IsAny<int>())).Returns(_fakeHotelRooms);
            _mockHotelsManager.Setup(m => m.GetHotelById(It.IsAny<int>())).Returns(_fakeHotels[0]);
            _mockHotelsManager.Setup(
                m =>
                    m.BookRoom(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(),
                        It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(1);

            _mockUsersManager.Setup(m => m.GetUserByLogin(It.IsAny<string>())).Returns(_fakeUser);

            _mockSecurityManager.Setup(m => m.GetCurrentUser()).Returns(_fakeUser.Login);

            _controller = new HotelBookingController(usersManager, hotelsManager, securityManager);
        }

        #endregion

        #region Test WebActions

        [TestMethod]
        public void Test_HotelBooking()
        {
            var actionResult = _controller.HotelBooking(1);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var viewResult = actionResult as ViewResult;
            Assert.IsNotNull(viewResult.Model);
            Assert.IsNull(viewResult.View);
            Assert.AreEqual(string.Empty, viewResult.MasterName);
            Assert.AreEqual(string.Empty, viewResult.ViewName);
            Assert.AreEqual(_fakeHotels[0].Name, viewResult.ViewData["HotelName"]);
            var model = viewResult.Model as BookingRoomModel;
            Assert.AreEqual(_fakeUser.FirstName, model.FirstName);
        }

        [TestMethod]
        public void Test_HotelBookingSubmit()
        {
            var model = new BookingRoomModel()
            {
                SelectedRoomType = 1,
                BeginDate = "2015-04-04",
                EndDate = "2015-07-07"
            };

            var result = _controller.HotelBookingSubmit(model) as ViewResult;

            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Once);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Once);
            _mockHotelsManager.Verify(x => x.BookRoom(_fakeUser.Id, model.SelectedRoomType, DateTime.Parse(model.BeginDate), DateTime.Parse(model.EndDate), model.Parking, model.Pool, model.Sauna, model.TennisCourt, model.Gym), Times.Once);
            Assert.AreEqual("RequetSend", result.ViewName);
        }

        [TestMethod]
        public void Test_HotelBookingSubmit_InValidModel()
        {
            var model = new BookingRoomModel()
            {
                SelectedRoomType = 1,
                BeginDate = "2015-04-04",
                EndDate = "2015-07-07"
            };

            _controller.ModelState.AddModelError("key", "error");

            var result = _controller.HotelBookingSubmit(model) as ViewResult;

            _mockHotelsManager.Verify(x => x.BookRoom(model.UserId, 1, DateTime.Parse(model.BeginDate), DateTime.Parse(model.EndDate), model.Parking, model.Pool, model.Sauna, model.TennisCourt, model.Gym), Times.Never);
            Assert.AreEqual("HotelBooking", result.ViewName);
        }

        #endregion

        #region Test Validators

        #region IsBeginDateValid

        [TestMethod]
        public void Test_IsBeginDateValid()
        {
            var date = "2015-04-04";
            var result = _controller.IsBeginDateValid(date);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginDateValid_1()
        {
            var date = "Monday, 3 August 2015";
            var result = _controller.IsBeginDateValid(date);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginDateValid_2()
        {
            var date = "Понеділок, 3 Серпня 2015";
            var result = _controller.IsBeginDateValid(date);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginDateValid_False()
        {
            var date = "2015-14-04";
            var result = _controller.IsBeginDateValid(date);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginDateValid_False_1()
        {
            var date = "Monday, 4 August 2015";
            var result = _controller.IsBeginDateValid(date);
            Assert.AreEqual(false, result.Data);
        }

        #endregion

        #region IsEndDateValid

        [TestMethod]
        public void Test_IsEndDateValid()
        {
            var beginDate = "2015-04-04";
            var endDate = "2015-04-07";
            var result = _controller.IsEndDateValid(endDate, beginDate);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsEndDateValid_1()
        {
            var beginDate = "Monday, 3 August 2015";
            var endDate = "2015-08-07";
            var result = _controller.IsEndDateValid(endDate, beginDate);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsEndDateValid_False()
        {
            var beginDate = "2015-04-20";
            var endDate = "2015-04-45";
            var result = _controller.IsEndDateValid(endDate, beginDate);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsEndDateValid_BadPeriod()
        {
            var beginDate = "2015-04-20";
            var endDate = "2015-04-10";
            var result = _controller.IsEndDateValid(endDate, beginDate);
            Assert.IsInstanceOfType(result.Data, typeof(string));
        }

        [TestMethod]
        public void Test_IsEndDateValid_BadPeriod_1()
        {
            var beginDate = "2015-04-19";
            var endDate = "2015-04-19";
            var result = _controller.IsEndDateValid(endDate, beginDate);
            Assert.IsInstanceOfType(result.Data, typeof(string));
        }

        #endregion

        #endregion
    }
}
