﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Hotel;
using CityCenter.WebUI.Frontend.Models.Panel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class HotelAdministratorPanelControllerTestMock
    {
        #region Const fields

        private const int TOTAL_HOTEL_RESERVATIONS = 6;

        private const int PENDING_RESERVATIONS_PAGE_SIZE = 2;

        private const int ACCEPTED_RESERVATIONS_PAGE_SIZE = 3;

        private const int PENDING_RESERVATIONS_COUNT = 4;

        private const int ACCEPTED_RESERVATIONS_COUNT = 4;

        #endregion

        #region Private Fields

        private Mock<IHotelsManager> _mockHotelsManager;
        private Mock<IUsersManager> _mockUsersManager;
        private Mock<ISecurityManager> _mockSecurityManager;
        private Mock<IEmailManager> _mockEmailManager;

        private HotelAdministratorPanelController _controller;

        private User _fakeAdmin;
        private List<HotelReservationDetails> _fakeHotelReservations;
        private List<HotelDescription> _fakeHotelDescriptions;
        private List<Hotel> _fakeHotels;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            #region Fake objects

            _fakeAdmin = new User
            {
                Id = 1,
                Login = "FakeAdmin",
                Password = "pass",
                FirstName = "admin",
                Surname = "admin",
                Email = "admin@gmail.com",
                Phone = "+340444444444"
            };
            _fakeHotels = new List<Hotel>
            {
                new Hotel
                {
                    Id = 1,
                    Name = "FakeHotel1"
                },
                new Hotel
                {
                    Id = 2,
                    Name = "FakeHotel1"
                }
            };
            _fakeHotelReservations = new List<HotelReservationDetails>
            {
                new HotelReservationDetails
                {
                    Id = 1,
                    HotelId = _fakeHotels[0].Id,
                    HotelName = _fakeHotels[0].Name,
                    Price = 1000M
                },
                new HotelReservationDetails
                {
                    Id = 2,
                    HotelId = _fakeHotels[0].Id,
                    HotelName = _fakeHotels[0].Name,
                    Price = 3740M
                }
            };
            _fakeHotelDescriptions = new List<HotelDescription>
            {
                new HotelDescription
                {
                    HotelId = _fakeHotels[0].Id,
                    Photo = new byte[] {123, 23, 32, 23},
                    PhotoExtension = "image/png"
                }
            };

            #endregion

            _mockHotelsManager = new Mock<IHotelsManager>();
            var hotelsManager = _mockHotelsManager.Object;

            _mockUsersManager = new Mock<IUsersManager>();
            var usersManager = _mockUsersManager.Object;

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockEmailManager = new Mock<IEmailManager>();
            var emailManager = _mockEmailManager.Object;

            _mockHotelsManager.Setup(m => m.GetHotelById(It.IsAny<int>())).Returns(_fakeHotels[0]);
            _mockHotelsManager.Setup(m => m.GetHotelDescriptionById(It.IsAny<int>())).Returns(_fakeHotelDescriptions[0]);
            _mockHotelsManager.Setup(m => m.GetPendingReservationCountByHotelId(It.IsAny<int>())).Returns(PENDING_RESERVATIONS_COUNT);
            _mockHotelsManager.Setup(m => m.GetReservationCountByHotelId(It.IsAny<int>())).Returns(ACCEPTED_RESERVATIONS_COUNT);
            _mockHotelsManager.Setup(m => m.GetPendingHotelReservationByHotelId(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeHotelReservations);
            _mockHotelsManager.Setup(m => m.GetHotelReservationByHotelId(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeHotelReservations);
            _mockHotelsManager.Setup(m => m.UpdateHotelDescription(It.IsAny<HotelDescription>()));
            _mockHotelsManager.Setup(m => m.UpdateHotelName(It.IsAny<string>(), It.IsAny<int>()));
            _mockHotelsManager.Setup(m => m.AcceptReservation(It.IsAny<int>()));
            _mockHotelsManager.Setup(m => m.DeclineReservation(It.IsAny<int>()));
            _mockHotelsManager.Setup(m => m.UpdateHotelImage(It.IsAny<HotelDescription>()));

            _mockUsersManager.Setup(m => m.GetUserByLogin(It.IsAny<string>())).Returns(_fakeAdmin);
            _mockUsersManager.Setup(m => m.GetUserById(It.IsAny<int>())).Returns(_fakeAdmin);
            _mockUsersManager.Setup(m => m.GetUserIdByReservationId(It.IsAny<int>())).Returns(_fakeAdmin.Id);
            _mockUsersManager.Setup(m => m.UpdateUser(It.IsAny<User>()));

            _mockSecurityManager.Setup(m => m.GetCurrentUser()).Returns(_fakeAdmin.Login);
            _mockSecurityManager.Setup(m => m.GetCurrentUserId()).Returns(_fakeAdmin.Id);
            _mockSecurityManager.Setup(m => m.IsUserAdministratorOfHotel(It.IsAny<int>(), It.IsAny<int>())).Returns((int userId, int hotelId) => userId == hotelId);

            _mockEmailManager.Setup(m => m.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()));

            _controller = new HotelAdministratorPanelController(usersManager, hotelsManager, securityManager, emailManager);
        }

        #endregion

        #region Web Actions

        [TestMethod]
        public void Test_EditHotelContent_HttpGet()
        {
            var actionResult = _controller.EditHotelContent(_fakeHotels[0].Id);
            _mockSecurityManager.Verify(x => x.GetCurrentUserId(), Times.Once);
            _mockSecurityManager.Verify(x => x.IsUserAdministratorOfHotel(_fakeAdmin.Id, _fakeHotels[0].Id), Times.Once);
            _mockHotelsManager.Verify(x => x.GetHotelById(_fakeHotels[0].Id), Times.Once);
            _mockHotelsManager.Verify(x => x.GetHotelDescriptionById(_fakeHotels[0].Id), Times.Once);
            _mockHotelsManager.Verify(x => x.GetPendingReservationCountByHotelId(_fakeHotels[0].Id), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));
            var result = actionResult as PartialViewResult;
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            Assert.IsNotNull(result.Model);
            var model = result.Model as HotelDescriptionModel;
            Assert.AreEqual(_fakeHotels[0].Name, model.HotelName);
            Assert.AreEqual(_fakeHotelDescriptions[0].HotelId, model.HotelDescription.HotelId);
        }

        [ExpectedException(typeof(HttpException))]
        [TestMethod]
        public void Test_EditHotelContent_HtppGet_Exception()
        {
            _controller.EditHotelContent(_fakeHotels[1].Id);
        }

        [TestMethod]
        public void Test_EditHotelContent_HttpPost()
        {
            var model = new HotelDescriptionModel
            {
                HotelName = _fakeHotels[0].Name,
                HotelDescription = _fakeHotelDescriptions[0]
            };
            _controller.ModelState.Clear();
            var actionResult = _controller.EditHotelContent(model);
            _mockHotelsManager.Verify(x => x.UpdateHotelDescription(model.HotelDescription), Times.Once);
            _mockHotelsManager.Verify(x => x.UpdateHotelName(model.HotelName, model.HotelDescription.HotelId), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Panel", result.RouteValues["action"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_EditHotelContent_HttpPost_Invalid()
        {
            var model = new HotelDescriptionModel
            {
                HotelName = _fakeHotels[0].Name,
                HotelDescription = _fakeHotelDescriptions[0]
            };
            _controller.ModelState.AddModelError("test", "test");
            var actionResult = _controller.EditHotelContent(model);
            _mockHotelsManager.Verify(x => x.UpdateHotelDescription(model.HotelDescription), Times.Never);
            _mockHotelsManager.Verify(x => x.UpdateHotelName(model.HotelName, model.HotelDescription.HotelId), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));
            var result = actionResult as PartialViewResult;
            Assert.AreEqual("_EditHotelContent", result.ViewName);
            Assert.IsNull(result.Model);
        }

        [TestMethod]
        public void Test_GetHotelImage()
        {
            var fileContentResult = _controller.GetHotelImage(1);
            Assert.IsInstanceOfType(fileContentResult, typeof(FileContentResult));
            Assert.IsNotNull(fileContentResult.FileContents);
        }

        [TestMethod]
        public void Test_UploadNewImage_NullImage()
        {
            var actionResult = _controller.UploadNewImage(_fakeHotels[0].Id, null);
            _mockHotelsManager.Verify(x => x.GetHotelDescriptionById(_fakeHotels[0].Id), Times.Never);
            _mockHotelsManager.Verify(x => x.UpdateHotelImage(_fakeHotelDescriptions[0]), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("EditHotelContent", result.RouteValues["action"]);
            Assert.AreEqual(_fakeHotels[0].Id, result.RouteValues["hotelId"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_PendingReservations()
        {
            var actionResult = _controller.PendingReservations(_fakeHotels[0].Id);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            Assert.IsNotNull(result.Model);
            var model = result.Model as AdminHotelReservationDetailsModel;
            Assert.AreEqual(_fakeHotelReservations[0].HotelName, model.ReservationDetails[0].HotelName);
            Assert.AreEqual(_fakeHotelReservations[0].Price, model.ReservationDetails[0].Price);
            Assert.AreEqual(_fakeHotelReservations[0].Id, model.ReservationDetails[0].Id);
            Assert.AreEqual(_fakeHotelReservations[0].HotelId, model.HotelId);
            Assert.AreEqual(_fakeHotelReservations[1].HotelName, model.ReservationDetails[1].HotelName);
            Assert.AreEqual(_fakeHotelReservations[1].Price, model.ReservationDetails[1].Price);
            Assert.AreEqual(_fakeHotelReservations[1].Id, model.ReservationDetails[1].Id);
            Assert.AreEqual(_fakeHotelReservations[1].HotelId, model.HotelId);
        }

        [ExpectedException(typeof(HttpException))]
        [TestMethod]
        public void Test_PendingReservations_Exception()
        {
            _controller.PendingReservations(_fakeHotels[1].Id);
        }

        [TestMethod]
        public void Test_AcceptedReservations()
        {
            var actionResult = _controller.AcceptedReservations(_fakeHotels[0].Id);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            Assert.IsNotNull(result.Model);
            var model = result.Model as AdminHotelReservationDetailsModel;
            Assert.AreEqual(_fakeHotelReservations[0].HotelName, model.ReservationDetails[0].HotelName);
            Assert.AreEqual(_fakeHotelReservations[0].Price, model.ReservationDetails[0].Price);
            Assert.AreEqual(_fakeHotelReservations[0].Id, model.ReservationDetails[0].Id);
            Assert.AreEqual(_fakeHotelReservations[0].HotelId, model.HotelId);
            Assert.AreEqual(_fakeHotelReservations[1].HotelName, model.ReservationDetails[1].HotelName);
            Assert.AreEqual(_fakeHotelReservations[1].Price, model.ReservationDetails[1].Price);
            Assert.AreEqual(_fakeHotelReservations[1].Id, model.ReservationDetails[1].Id);
            Assert.AreEqual(_fakeHotelReservations[1].HotelId, model.HotelId);
        }

        [ExpectedException(typeof(HttpException))]
        [TestMethod]
        public void Test_AcceptedReservations_Exception()
        {
            _controller.AcceptedReservations(_fakeHotels[1].Id);
        }

        [TestMethod]
        public void Test_AcceptBook()
        {
            var actionResult = _controller.AcceptBook(_fakeHotelReservations[0].Id, _fakeHotelReservations[0].HotelId);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("PendingReservations", result.RouteValues["action"]);
            Assert.AreEqual(_fakeHotelReservations[0].HotelId, result.RouteValues["hotelId"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_DeclineBook()
        {
            var actionResult = _controller.DeclineBook(_fakeHotelReservations[0].Id, _fakeHotelReservations[0].HotelId);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("PendingReservations", result.RouteValues["action"]);
            Assert.AreEqual(_fakeHotelReservations[0].HotelId, result.RouteValues["hotelId"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        #endregion
    }
}
