﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Excursion;
using CityCenter.WebUI.Frontend.Models.Panel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class ExcursionAdministratorPanelControllerTestMock
    {
        #region Const fields

        private const int TOTAL_EXCURSION_RESERVATIONS = 6;

        private const int EXCURSION_RESERVATIONS_PAGE_SIZE = 3;

        private const int PENDING_RESERVATIONS_COUNT = 3;

        private const int ACCEPTED_RESERVATIONS_COUNT = 3;

        #endregion

        #region Private Fields

        private Mock<ISecurityManager> _mockSecurityManager;
        private Mock<IUsersManager> _mockUsersManager;
        private Mock<IExcursionsManager> _mockExcursionsManager;
        private Mock<IEmailManager> _mockEmailManager;

        private ExcursionAdministratorPanelController _controller;

        private User _fakeUser;
        private List<ExcursionReservationDetails> _fakeExcursionReservation;
        private List<ExcursionDescription> _fakeExcursionDescriptions;
        private List<Excursion> _fakeExcursions;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            #region Fake objects

            _fakeUser = new User()
            {
                Id = 1,
                Login = "FakeLogin",
                Password = "pass",
                FirstName = "Fake",
                Surname = "User",
                Email = "fake@gmail.com",
                Phone = "+340444444444"
            };
            _fakeExcursionReservation = new List<ExcursionReservationDetails>
            {
                new ExcursionReservationDetails
                {
                    UserId = _fakeUser.Id,
                    ExcursionId = 1,
                    ExcursionName = "Fake 1",
                    Price = 100M,
                    ReservationId = 1
                    
                },
                new ExcursionReservationDetails
                {
                    UserId = _fakeUser.Id,
                    ExcursionId = 1,
                    ExcursionName = "Fake 2",
                    Price = 90M,
                    ReservationId = 2
                }
            };

            _fakeExcursions = new List<Excursion>()
            {
                new Excursion()
                {
                    Address = "FakeAddress1",
                    BeginTime = new TimeSpan(12, 00, 00),
                    Duration = new TimeSpan(02, 00, 00),
                    Id = 1,
                    Name = "Fake 1",
                    Price = 100M
                },

                new Excursion()
                {
                    Address = "FakeAddress2",
                    BeginTime = new TimeSpan(16, 00, 00),
                    Duration = new TimeSpan(01, 30, 00),
                    Id = 2,
                    Name = "Fake 2",
                    Price = 90M
                }
            };

            _fakeExcursionDescriptions = new List<ExcursionDescription>()
            {
                new ExcursionDescription()
                {
                    ExcursionId = _fakeExcursions[0].Id,
                    LongDescription = "Fake Long Description",
                    Photo = new byte[] {123, 23, 32, 23},
                    PhotoExtension = "image/png",
                    ShortDescription = "Fake Short Description"
                }
            };

            #endregion

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockUsersManager = new Mock<IUsersManager>();
            var usersManager = _mockUsersManager.Object;

            _mockExcursionsManager = new Mock<IExcursionsManager>();
            var excursionManager = _mockExcursionsManager.Object;

            _mockEmailManager = new Mock<IEmailManager>();
            var emailManager = _mockEmailManager.Object;

            _mockExcursionsManager.Setup(m => m.GetExcursionsByAdminId(It.IsAny<int>())).Returns(_fakeExcursions);
            _mockExcursionsManager.Setup(m => m.GetAcceptedReservationsCountByExcursionId(It.IsAny<int>())).Returns(ACCEPTED_RESERVATIONS_COUNT);
            _mockExcursionsManager.Setup(m => m.GetPendingReservationsCountByExcursionId(It.IsAny<int>())).Returns(PENDING_RESERVATIONS_COUNT);
            _mockExcursionsManager.Setup(m => m.GetPendingReservationsByExcursionId(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeExcursionReservation);
            _mockExcursionsManager.Setup(m => m.GetAcceptedReservationsByExcursionId(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeExcursionReservation);
            _mockExcursionsManager.Setup(m => m.GetReservationDetailsCountByUserId(It.IsAny<int>())).Returns(TOTAL_EXCURSION_RESERVATIONS);
            _mockExcursionsManager.Setup(m => m.GetReservationDetailsByUserId(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeExcursionReservation);
            _mockExcursionsManager.Setup(m => m.GetExcursionDescriptionById(It.IsAny<int>()))
                .Returns(_fakeExcursionDescriptions[0]);
            _mockExcursionsManager.Setup(m => m.GetExcursionById(It.IsAny<int>())).Returns(_fakeExcursions[0]);
            _mockExcursionsManager.Setup(m => m.UpdateExcursionDescription(It.IsAny<ExcursionDescription>()));
            _mockExcursionsManager.Setup(m => m.UpdateExcursionName(It.IsAny<string>(), It.IsAny<int>()));
            _mockExcursionsManager.Setup(m => m.AcceptReservation(It.IsAny<int>()));
            _mockExcursionsManager.Setup(m => m.DeclineReservation(It.IsAny<int>()));
            _mockExcursionsManager.Setup(m => m.UpdateExcursionImage(It.IsAny<ExcursionDescription>()));

            _mockSecurityManager.Setup(m => m.GetCurrentUserId()).Returns(_fakeUser.Id);
            _mockSecurityManager.Setup(m => m.IsUserAdministratorOfExcursion(It.IsAny<int>(), It.IsAny<int>())).Returns((int userId, int excursionId) => userId == excursionId);


            _mockUsersManager.Setup(m => m.GetUserById(It.IsAny<int>())).Returns(_fakeUser);

            _controller = new ExcursionAdministratorPanelController(excursionManager, securityManager, usersManager, emailManager);
        }


        #endregion

        #region Test WebActions

        [TestMethod]
        public void Test_Edit_Excursion_Content_HttpGet()
        {
            var actionResult = _controller.EditExcursionContent(1);
            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));
            var result = actionResult as PartialViewResult;
            Assert.IsNotNull(result.Model);
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            var model = result.Model as ExcursionDescriptionModel;
            Assert.AreEqual(_fakeExcursions[0].Name, model.Name);
            Assert.AreEqual(_fakeExcursions[0].BeginTime, model.Excursion.BeginTime);
            Assert.AreEqual(_fakeExcursions[0].Price, model.Excursion.Price);
            Assert.AreEqual(_fakeExcursions[0].Duration, model.Excursion.Duration);
        }

        [TestMethod]
        public void Test_Edit_Excursion_Content_HttpPost()
        {
            var model = new ExcursionDescriptionModel()
            {
                Name = _fakeExcursions[0].Name,
                Description = _fakeExcursionDescriptions[0],
                Excursion = _fakeExcursions[0]
            };
            _controller.ModelState.Clear();
            var actionResult = _controller.EditExcursionContent(model);
            _mockExcursionsManager.Verify(x => x.UpdateExcursionDescription(model.Description), Times.Once);
            _mockExcursionsManager.Verify(x => x.UpdateExcursionName(model.Name, model.Excursion.Id), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Panel", result.RouteValues["action"]);
        }

        [TestMethod]
        public void Test_Edit_Excursion_Content_HttpPost_InValid()
        {
            var model = new ExcursionDescriptionModel()
            {
                Name = _fakeExcursions[0].Name,
                Description = _fakeExcursionDescriptions[0],
                Excursion = _fakeExcursions[0]
            };
            _controller.ModelState.AddModelError("error", "testError");
            var actionResult = _controller.EditExcursionContent(model);
            _mockExcursionsManager.Verify(x => x.UpdateExcursionDescription(model.Description), Times.Never);
            _mockExcursionsManager.Verify(x => x.UpdateExcursionName(model.Name, model.Excursion.Id), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Test_GetExcursionImage()
        {
            var fileContentResult = _controller.GetHotelImage(1);
            Assert.IsInstanceOfType(fileContentResult, typeof(FileContentResult));
            Assert.IsNotNull(fileContentResult.FileContents);
        }

        [TestMethod]
        public void Test_UploadNewImage_NullImage()
        {
            var actionResult = _controller.UploadNewImage(_fakeExcursions[0].Id, null);
            _mockExcursionsManager.Verify(x => x.GetExcursionDescriptionById(_fakeExcursions[0].Id), Times.Never);
            _mockExcursionsManager.Verify(x => x.UpdateExcursionImage(_fakeExcursionDescriptions[0]), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("EditExcursionContent", result.RouteValues["action"]);
            Assert.AreEqual(_fakeExcursions[0].Id, result.RouteValues["excursionId"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_PendingReservations()
        {
            var actionResult = _controller.PendingReservations(_fakeExcursions[0].Id);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            Assert.IsNotNull(result.Model);
            var model = result.Model as AdminExcursionReservationDetailsModel;
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionName, model.ReservationDetails[0].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[0].Price, model.ReservationDetails[0].ReservationDetails.Price);
            Assert.AreEqual(_fakeExcursionReservation[0].ReservationId, model.ReservationDetails[0].ReservationDetails.ReservationId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionName, model.ReservationDetails[1].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[1].Price, model.ReservationDetails[1].ReservationDetails.Price);
            Assert.AreEqual(_fakeExcursionReservation[1].ReservationId, model.ReservationDetails[1].ReservationDetails.ReservationId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionId, model.ExcursionId);
        }

        [TestMethod]
        public void Test_Pending_Reservations()
        {
            var actionResult = _controller.PendingReservations(_fakeExcursions[0].Id);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            Assert.IsNotNull(result.Model);
            var model = result.Model as AdminExcursionReservationDetailsModel;
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionName, model.ReservationDetails[0].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[0].Price, model.ReservationDetails[0].ReservationDetails.Price);
            Assert.AreEqual(_fakeExcursionReservation[0].ReservationId, model.ReservationDetails[0].ReservationDetails.ReservationId);
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionId, model.ExcursionId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionName, model.ReservationDetails[1].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[1].Price, model.ReservationDetails[1].ReservationDetails.Price);
            Assert.AreEqual(_fakeExcursionReservation[1].ReservationId, model.ReservationDetails[1].ReservationDetails.ReservationId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionId, model.ExcursionId);
        }

        [ExpectedException(typeof(HttpException))]
        [TestMethod]
        public void Test_PendingReservations_Exception()
        {
            _controller.PendingReservations(_fakeExcursions[1].Id);
        }

        [TestMethod]
        public void Test_Accepted_Reservations()
        {
            var actionResult = _controller.AcceptedReservations(_fakeExcursions[0].Id);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual(PENDING_RESERVATIONS_COUNT, result.ViewData["CountOfPendingReservation"]);
            Assert.IsNotNull(result.Model);
            var model = result.Model as AdminExcursionReservationDetailsModel;
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionName, model.ReservationDetails[0].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[0].Price, model.ReservationDetails[0].ReservationDetails.Price);
            Assert.AreEqual(_fakeExcursionReservation[0].ReservationId, model.ReservationDetails[0].ReservationDetails.ReservationId);
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionId, model.ExcursionId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionName, model.ReservationDetails[1].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[1].Price, model.ReservationDetails[1].ReservationDetails.Price);
            Assert.AreEqual(_fakeExcursionReservation[1].ReservationId, model.ReservationDetails[1].ReservationDetails.ReservationId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionId, model.ExcursionId);
        }

        [ExpectedException(typeof(HttpException))]
        [TestMethod]
        public void Test_AcceptedReservations_Exception()
        {
            _controller.AcceptedReservations(_fakeExcursions[1].Id);
        }

        [TestMethod]
        public void Test_AcceptBook()
        {
            var actionResult = _controller.AcceptReservation(_fakeExcursionReservation[0].ReservationId, _fakeExcursionReservation[0].ExcursionId);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("PendingReservations", result.RouteValues["action"]);
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionId, result.RouteValues["excursionId"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_DeclineBook()
        {
            var actionResult = _controller.DeclineReservation(_fakeExcursionReservation[0].ReservationId, _fakeExcursionReservation[0].ExcursionId);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("PendingReservations", result.RouteValues["action"]);
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionId, result.RouteValues["excursionId"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        #endregion
    }
}
