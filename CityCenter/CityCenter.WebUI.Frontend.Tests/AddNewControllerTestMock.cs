﻿using System.Collections.Generic;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Excursion;
using CityCenter.WebUI.Frontend.Models.Hotel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class AddNewControllerTestMock
    {
        #region Private Fields

        private Mock<IHotelsManager> _mockHotelsManager;
        private Mock<IExcursionsManager> _mockExcursionsManager;
        private Mock<ISecurityManager> _mockSecurityManager;

        private AddNewController _controller;

        private User _fakeAdmin;
        private NewExcursionModel _fakeNewExcursion;
        private AddNewHotelModel _fakeNewHotel;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            #region Fake objects

            _fakeAdmin = new User
            {
                Id = 1,
                Login = "FakeAdmin",
                Password = "pass",
                FirstName = "admin",
                Surname = "admin",
                Email = "admin@gmail.com",
                Phone = "+340444444444"
            };
            _fakeNewExcursion = new NewExcursionModel
            {
                Name = "Fake",
                Address = "Fake",
                BeginTime = "19:00",
                Duration = "01:30",
                Price = 50M,
                ShortDescription = "Short",
                LongDescription = "Long"
            };
            _fakeNewHotel = new AddNewHotelModel
            {
                Name = "Fake",
                Address = "Fake",
                Rating = 4,
                Header = "Fake header",
                Description = "Fake Description",
                Price1 = 500M,
                RoomsCount1 = 7,
                RoomType1 = "Fake room"
            };

            #endregion

            _mockHotelsManager = new Mock<IHotelsManager>();
            var hotelsManager = _mockHotelsManager.Object;

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockExcursionsManager = new Mock<IExcursionsManager>();
            var excursionsManager = _mockExcursionsManager.Object;

            _mockHotelsManager.Setup(m => m.AddNewHotel(It.IsAny<int>(), It.IsAny<Hotel>(), It.IsAny<List<HotelRooms>>(), It.IsAny<HotelDescription>())).Returns(1);

            _mockExcursionsManager.Setup(m => m.AddNewExcursion(It.IsAny<Excursion>(), It.IsAny<ExcursionDescription>(), It.IsAny<int>())).Returns(1);

            _mockSecurityManager.Setup(m => m.GetCurrentUserId()).Returns(_fakeAdmin.Id);

            _controller = new AddNewController(excursionsManager, securityManager, hotelsManager);
        }

        #endregion

        #region Web Actions

        [TestMethod]
        public void Test_AddNewExcursion_InValid()
        {
            _controller.ModelState.AddModelError("test", "test");
            var actionResult = _controller.AddNewExcursion(_fakeNewExcursion);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual("NewExcursion", result.ViewName);
            Assert.IsNotNull(result.Model);
            var model = result.Model as NewExcursionModel;
            Assert.AreEqual(_fakeNewExcursion.Name, model.Name);
            Assert.AreEqual(_fakeNewExcursion.Address, model.Address);
            Assert.AreEqual(_fakeNewExcursion.Price, model.Price);
            Assert.AreEqual(_fakeNewExcursion.ShortDescription, model.ShortDescription);
            Assert.AreEqual(_fakeNewExcursion.LongDescription, model.LongDescription);
        }

        [TestMethod]
        public void Test_AddNewHotel_InValid()
        {
            _controller.ModelState.AddModelError("test", "test");
            var actionResult = _controller.AddNewHotel(_fakeNewHotel);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual("NewHotel", result.ViewName);
            Assert.IsNotNull(result.Model);
            var model = result.Model as AddNewHotelModel;
            Assert.AreEqual(_fakeNewHotel.Name, model.Name);
            Assert.AreEqual(_fakeNewHotel.Address, model.Address);
            Assert.AreEqual(_fakeNewHotel.Rating, model.Rating);
            Assert.AreEqual(_fakeNewHotel.Header, model.Header);
            Assert.AreEqual(_fakeNewHotel.Description, model.Description);
            Assert.AreEqual(_fakeNewHotel.RoomType1, model.RoomType1);
            Assert.AreEqual(_fakeNewHotel.Price1, model.Price1);
            Assert.AreEqual(_fakeNewHotel.RoomsCount1, model.RoomsCount1);
        }

        #endregion

        #region Validation

        #region IsDurationValid

        [TestMethod]
        public void Test_IsDurationValid_1()
        {
            var duration = "1:3";
            var result = _controller.IsDurationValid(duration);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsDurationValid_2()
        {
            var duration = "1:30";
            var result = _controller.IsDurationValid(duration);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsDurationValid_3()
        {
            var duration = "01:3";
            var result = _controller.IsDurationValid(duration);
            Assert.AreEqual(true, result.Data);
        }
        
        [TestMethod]
        public void Test_IsDurationValid_False_1()
        {
            var duration = "25:30";
            var result = _controller.IsDurationValid(duration);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsDurationValid_False_2()
        {
            var duration = "11-33";
            var result = _controller.IsDurationValid(duration);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsDurationValid_False_3()
        {
            var duration = "12:63";
            var result = _controller.IsDurationValid(duration);
            Assert.AreEqual(false, result.Data);
        }

        #endregion

        #region IsBeginTimeValid

        public void Test_IsBeginTimeValid_1()
        {
            var duration = "1:3";
            var result = _controller.IsBeginTimeValid(duration);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginTimeValid_2()
        {
            var duration = "1:30";
            var result = _controller.IsBeginTimeValid(duration);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginTimeValid_3()
        {
            var duration = "01:3";
            var result = _controller.IsBeginTimeValid(duration);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginTimeValid_False_1()
        {
            var duration = "25:30";
            var result = _controller.IsBeginTimeValid(duration);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginTimeValid_False_2()
        {
            var duration = "11-33";
            var result = _controller.IsBeginTimeValid(duration);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsBeginTimeValid_False_3()
        {
            var duration = "12:63";
            var result = _controller.IsBeginTimeValid(duration);
            Assert.AreEqual(false, result.Data);
        }


        #endregion

        #endregion
    }
}
