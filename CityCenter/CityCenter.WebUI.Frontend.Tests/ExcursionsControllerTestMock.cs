﻿using System.Collections.Generic;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Excursion;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PagedList;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class ExcursionsControllerTestMock
    {

        #region Const fields

        private const int TOTAL_EXCURSION_DESCRIPTIONS = 3;

        private const int TOTAL_PAGE_SIZE = 2;

        #endregion

        #region Private fields

        private Mock<IExcursionsManager> _mockExcursionManager;
        private ExcursionsController _controller;

        private List<ExcursionDescription> _fakeExcursionDescriptions;
        private List<Excursion> _fakeExcursions;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            _fakeExcursions = new List<Excursion>()
            {
                new Excursion()
                {
                   Id = 1,
                   Name = "FakeExcursion1"
                }
            };

            _fakeExcursionDescriptions = new List<ExcursionDescription>()
            {
                new ExcursionDescription()
                {
                    ExcursionId = _fakeExcursions[0].Id,
                    Photo = new byte[] {123, 23, 32, 23},
                    PhotoExtension = "image/png"
                }
            };

            _mockExcursionManager = new Mock<IExcursionsManager>();
            var excursionManager = _mockExcursionManager.Object;

            _mockExcursionManager.Setup(m => m.GetExcursionDescriptionsCount()).Returns(TOTAL_EXCURSION_DESCRIPTIONS);
            _mockExcursionManager.Setup(m => m.GetExcursionDescriptions(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(_fakeExcursionDescriptions);
            _mockExcursionManager.Setup(m => m.GetExcursionDescriptionById(It.IsAny<int>()))
                .Returns(_fakeExcursionDescriptions[0]);
            _mockExcursionManager.Setup(m => m.GetExcursionById(It.IsAny<int>())).Returns(_fakeExcursions[0]);

            _controller = new ExcursionsController(excursionManager);
        }

        #endregion

        #region WebActions

        [TestMethod]
        public void Test_Excursions()
        {
            var actionResult = _controller.Excursions();
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsNull(result.View);
            var model = result.Model as StaticPagedList<ExcursionDescriptionModel>;
            Assert.AreEqual(_fakeExcursions[0].Id, model[0].Excursion.Id);
            Assert.AreEqual(_fakeExcursions[0].Name, model[0].Name);
        }

        [TestMethod]
        public void Test_GetExcursionImage()
        {
            var fileContentResult = _controller.GetExcursionImage(1);
            Assert.IsInstanceOfType(fileContentResult, typeof(FileContentResult));
            Assert.IsNotNull(fileContentResult.FileContents);
        }
        [TestMethod]
        public void Test_Excursions_InvalidPage_1()
        {
            var actionResult = _controller.Excursions(page: 10);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Excursions", result.RouteValues["action"]);
            Assert.AreEqual(TOTAL_PAGE_SIZE, result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);

        }

        [TestMethod]
        public void Test_Excursions_InvalidPage_2()
        {
            var actionResult = _controller.Excursions(page: -1);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Excursions", result.RouteValues["action"]);
            Assert.IsNull(result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_ExcursionInfo()
        {
            var actionResult = _controller.ExcursionInfo(_fakeExcursions[0].Id);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsNull(result.View);
            var model = result.Model as ExcursionDescriptionModel;
            Assert.AreEqual(_fakeExcursions[0].Id, model.Excursion.Id);
            Assert.AreEqual(_fakeExcursions[0].Name, model.Name);
        }

        #endregion
    }
}
