﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class AccountControllerTestMock
    {
        #region Const fields

        private const string FAKE_RETURN_URL = "returnUrl";

        #endregion

        #region Private Fields

        private Mock<ISecurityManager> _mockSecurityManager;
        private Mock<IUsersManager> _mockUsersManager;
        private Mock<IEmailManager> _mockEmailManager;

        private User _fakeUser;

        private AccountController _controller;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            #region Fake objects

            _fakeUser = new User()
            {
                Id = 1,
                Login = "FakeLogin",
                Password = "pass",
                FirstName = "Fake",
                Surname = "User",
                Email = "fake@gmail.com",
                Phone = "+340444444444"
            };
            #endregion

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockUsersManager = new Mock<IUsersManager>();
            var usersManager = _mockUsersManager.Object;

            _mockEmailManager = new Mock<IEmailManager>();
            var emailManager = _mockEmailManager.Object;

            _mockSecurityManager.Setup(m => m.IsAuthenticated()).Returns(_fakeUser != null);
            _mockSecurityManager.Setup(m => m.Authentication(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            _controller = new AccountController(usersManager, securityManager, emailManager);
        }

        #endregion

        #region Test WebActions

        [TestMethod]
        public void Test_LogIn_HttpGet_IsAuthenticated_HttpGet()
        {
            var actionResult = _controller.LogIn(FAKE_RETURN_URL);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("MainPage", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_LogIn_NoAuthenticated_HttpGet()
        {
            _fakeUser.Login = "fake";
            var actionResult = _controller.LogIn(FAKE_RETURN_URL);
            _mockSecurityManager.Verify(x => x.IsAuthenticated(), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("MainPage", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_LogIn_HttpPost()
        {
            var model = new UserLogInModel()
            {
                Login = _fakeUser.Login,
                Password = _fakeUser.Password
            };

            var actionResult = _controller.LogIn(model, FAKE_RETURN_URL);
            _mockSecurityManager.Verify(x => x.Authentication(model.Login, model.Password), Times.Once);

            Assert.IsInstanceOfType(actionResult, typeof(RedirectResult));
            var result = actionResult as RedirectResult;
            Assert.AreEqual(FAKE_RETURN_URL != null, true);
            Assert.AreEqual(FAKE_RETURN_URL, result.Url);
        }

        #endregion
    }
}
