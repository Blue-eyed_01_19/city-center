﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Excursion;
using CityCenter.WebUI.Frontend.Models.Hotel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class ExcursionRegistrationControllerTestMock
    {
        #region Private fields

        private Mock<ISecurityManager> _mockSecurityManager;
        private Mock<IUsersManager> _mockUsersManager;
        private Mock<IExcursionsManager> _mockExcursionsManager;
        private ExcursionRegistrationController _controller;

        private User _fakeUser;
        private List<Excursion> _fakeExcursions;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            _fakeUser = new User()
            {
                Id = 1,
                Login = "FakeLogin",
                FirstName = "Fake",
                Surname = "User",
                Email = "fake@gmail.com",
                Phone = "+340444444444"
            };
            _fakeExcursions = new List<Excursion>()
            {
                new Excursion() {Id = 1, Name = "Fake Excursion"}
            };

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockUsersManager = new Mock<IUsersManager>();
            var usersManager = _mockUsersManager.Object;

            _mockExcursionsManager = new Mock<IExcursionsManager>();
            var excursionsManager = _mockExcursionsManager.Object;

            _mockExcursionsManager.Setup(m => m.GetExcursions()).Returns(_fakeExcursions);
            _mockExcursionsManager.Setup(m => m.GetExcursionById(It.IsAny<int>())).Returns(_fakeExcursions[0]);
            _mockExcursionsManager.Setup(
                m =>
                    m.AddNewReservation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(1);

            _mockUsersManager.Setup(m => m.GetUserById(It.IsAny<int>())).Returns(_fakeUser);

            _mockSecurityManager.Setup(m => m.GetCurrentUserId()).Returns(_fakeUser.Id);

            _mockSecurityManager.Setup(m => m.GetCurrentUser()).Returns(_fakeUser.Login);

            _mockUsersManager.Setup(m => m.GetUserByLogin(It.IsAny<string>())).Returns(_fakeUser);

            _controller = new ExcursionRegistrationController(securityManager, usersManager, excursionsManager);
        }

        #endregion

        #region Test WebActions

        [TestMethod]
        public void Test_Excursion_Reservation()
        {
            var actionResult = _controller.ExcursionRegistration(1);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var viewResult = actionResult as ViewResult;
            Assert.IsNotNull(viewResult.Model);
            Assert.IsNull(viewResult.View);
            Assert.AreEqual(string.Empty, viewResult.MasterName);
            Assert.AreEqual(string.Empty, viewResult.ViewName);
            Assert.AreEqual(_fakeExcursions[0].Name, viewResult.ViewData["Name"]);
            var model = viewResult.Model as ExcursionRegistrationModel;
            Assert.AreEqual(_fakeUser.FirstName, model.FirstName);
        }

        [TestMethod]
        public void Test_Excursion_Reservation_Submit()
        {
            var model = new ExcursionRegistrationModel()
            {
                Date = "2015-08-08",
                ExcursionId = _fakeExcursions[0].Id
            };

            var result = _controller.ExcursionRegistration(model) as ViewResult;

            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Once);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Once);
            _mockExcursionsManager.Verify(x => x.AddNewReservation(_fakeUser.Id, model.ExcursionId, DateTime.Parse(model.Date)), Times.Once);
            Assert.AreEqual("RequetSend", result.ViewName);
        }

        [TestMethod]
        public void Test_Excursion_Reservation_InValidModel()
        {
            var model = new ExcursionRegistrationModel()
            {
                Date = "2015-08-08",
                ExcursionId = _fakeExcursions[0].Id
            };

            _controller.ModelState.AddModelError("key", "error");

            var result = _controller.ExcursionRegistration(model) as ViewResult;

            _mockExcursionsManager.Verify(x => x.AddNewReservation(_fakeUser.Id, model.ExcursionId, DateTime.Parse(model.Date)), Times.Never);
            Assert.AreEqual("ExcursionRegistration", result.ViewName);
        }

        #endregion

        #region Test Validators

        [TestMethod]
        public void Test_IsDateValid()
        {
            var date = "2015-08-08";
            var result = _controller.IsDateValid(date);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsDateValid_1()
        {
            var date = "Saturday, 8 August 2015";
            var result = _controller.IsDateValid(date);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsDateValid_2()
        {
            var date = "Субота, 8 Серпня 2015";
            var result = _controller.IsDateValid(date);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsDateValid_False()
        {
            var date = "2015-16-08";
            var result = _controller.IsDateValid(date);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsDateValid_False_1()
        {
            var date = "Monday, 4 August 2015";
            var result = _controller.IsDateValid(date);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsDateValid_False_2()
        {
            var date = "2015-08-38";
            var result = _controller.IsDateValid(date);
            Assert.AreEqual(false, result.Data);
        }

        #endregion
    }
}
