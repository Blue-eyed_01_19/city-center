﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Controllers;
using CityCenter.WebUI.Frontend.Models.Account;
using CityCenter.WebUI.Frontend.Models.Panel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PagedList;

namespace CityCenter.WebUI.Frontend.Tests
{
    [TestClass]
    public class UserPanelControllerTestMock
    {
        #region Const fields

        private const int TOTAL_HOTEL_RESERVATIONS = 6;

        private const int HOTEL_RESERVATIONS_PAGE_SIZE = 3;

        private const int TOTAL_EXCURSION_RESERVATIONS = 6;

        private const int HOTEL_EXCURSION_PAGE_SIZE = 3;

        #endregion

        #region Private Fields

        private Mock<IHotelsManager> _mockHotelsManager;
        private Mock<IUsersManager> _mockUsersManager;
        private Mock<ISecurityManager> _mockSecurityManager;
        private Mock<IExcursionsManager> _mockExcursionsManager;

        private UserPanelController _controller;

        private User _fakeUser;
        private List<HotelReservationDetails> _fakeHotelReservations;
        private List<ExcursionReservationDetails> _fakeExcursionReservation;

        #endregion

        #region Initialize

        [TestInitialize]
        public void SetUp()
        {
            _fakeUser = new User()
            {
                Id = 1,
                Login = "FakeLogin",
                Password = "pass",
                FirstName = "Fake",
                Surname = "User",
                Email = "fake@gmail.com",
                Phone = "+340444444444"
            };
            _fakeHotelReservations = new List<HotelReservationDetails>
            {
                new HotelReservationDetails
                {
                    HotelId = 1,
                    HotelName = "Fake 1"
                },
                new HotelReservationDetails
                {
                    HotelId = 2,
                    HotelName = "Fake 2"
                }
            };
            _fakeExcursionReservation = new List<ExcursionReservationDetails>
            {
                new ExcursionReservationDetails
                {
                    UserId = _fakeUser.Id,
                    ExcursionId = 1,
                    ExcursionName = "Fake 1" 
                    
                },
                new ExcursionReservationDetails
                {
                    UserId = _fakeUser.Id,
                    ExcursionId = 2,
                    ExcursionName = "Fake 2" 
                }
            };

            _mockHotelsManager = new Mock<IHotelsManager>();
            var hotelsManager = _mockHotelsManager.Object;

            _mockUsersManager = new Mock<IUsersManager>();
            var usersManager = _mockUsersManager.Object;

            _mockSecurityManager = new Mock<ISecurityManager>();
            var securityManager = _mockSecurityManager.Object;

            _mockExcursionsManager = new Mock<IExcursionsManager>();
            var excursionManager = _mockExcursionsManager.Object;

            _mockHotelsManager.Setup(m => m.GetReservationCount(It.IsAny<int>())).Returns(TOTAL_HOTEL_RESERVATIONS);
            _mockHotelsManager.Setup(m => m.GetHotelReservationDetails(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeHotelReservations);

            _mockExcursionsManager.Setup(m => m.GetReservationDetailsCountByUserId(It.IsAny<int>())).Returns(TOTAL_EXCURSION_RESERVATIONS);
            _mockExcursionsManager.Setup(m => m.GetReservationDetailsByUserId(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(_fakeExcursionReservation);


            _mockUsersManager.Setup(m => m.GetUserByLogin(It.IsAny<string>())).Returns(_fakeUser);
            _mockUsersManager.Setup(m => m.UpdateUser(It.IsAny<User>()));

            _mockSecurityManager.Setup(m => m.GetCurrentUser()).Returns(_fakeUser.Login);
            _mockSecurityManager.Setup(m => m.UpdateUserPassword(It.IsAny<User>()));
            _mockSecurityManager.Setup(m => m.CheckPassword(It.IsAny<string>(), It.IsAny<string>())).Returns(
                (string realPass, string pass) => pass == _fakeUser.Password);

            _controller = new UserPanelController(usersManager, securityManager, hotelsManager, excursionManager);
        }

        #endregion

        #region Test WebActions

        [TestMethod]
        public void Test_Info_HtppGet()
        {
            var actionResult = _controller.Info();
            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Once);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            var model = result.Model as PanelUserInfoModel;
            Assert.AreEqual(_fakeUser.FirstName, model.FirstName);
            Assert.AreEqual(_fakeUser.Surname, model.Surname);
            Assert.AreEqual(_fakeUser.Email, model.Email);
            Assert.AreEqual(_fakeUser.Phone, model.Phone);
        }

        [TestMethod]
        public void Test_Info_HttpPost()
        {
            var postModel = new PanelUserInfoModel
            {
                FirstName = _fakeUser.FirstName,
                Surname = _fakeUser.Surname,
                Email = _fakeUser.Email,
                Phone = _fakeUser.Phone,
            };
            _controller.ModelState.Clear();
            var actionResult = _controller.Info(postModel);
            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Once);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Once);
            _mockUsersManager.Verify(x => x.UpdateUser(_fakeUser), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Panel", result.RouteValues["action"]);
            Assert.AreEqual("UserPanel", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_Info_HttpPost_InValid()
        {
            var postModel = new PanelUserInfoModel
            {
                FirstName = _fakeUser.FirstName,
                Surname = _fakeUser.Surname,
                Email = _fakeUser.Email,
                Phone = _fakeUser.Phone,
            };
            _controller.ModelState.AddModelError("test", "test");
            var actionResult = _controller.Info(postModel);
            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Never);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Never);
            _mockUsersManager.Verify(x => x.UpdateUser(_fakeUser), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual("Info", result.ViewName);
            Assert.IsNotNull(result.Model);
            var newModel = result.Model as PanelUserInfoModel;
            Assert.AreEqual(_fakeUser.FirstName, newModel.FirstName);
            Assert.AreEqual(_fakeUser.Surname, newModel.Surname);
            Assert.AreEqual(_fakeUser.Email, newModel.Email);
            Assert.AreEqual(_fakeUser.Phone, newModel.Phone);
        }

        [TestMethod]
        public void Test_ChangePassword_HttpPost()
        {
            var postModel = new СhangePasswordModel
            {
                OldPassword = "pass",
                NewPassword = "newpass",
                NewPasswordConfirmation = "newpass"
            };
            _controller.ModelState.Clear();
            var actionResult = _controller.ChangePassword(postModel);
            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Once);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Once);
            _mockSecurityManager.Verify(x => x.UpdateUserPassword(_fakeUser), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Panel", result.RouteValues["action"]);
            Assert.AreEqual("UserPanel", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_ChangePassword_HttpPost_InValid()
        {
            _controller.ModelState.AddModelError("test", "test");
            var actionResult = _controller.ChangePassword(new СhangePasswordModel());
            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Never);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Never);
            _mockSecurityManager.Verify(x => x.UpdateUserPassword(_fakeUser), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual("ChangePassword", result.ViewName);
            Assert.IsNull(result.Model);
        }

        [TestMethod]
        public void Test_Feedback_HttpGet()
        {
            var actionResult = _controller.Feedback();
            _mockSecurityManager.Verify(x => x.GetCurrentUser(), Times.Once);
            _mockUsersManager.Verify(x => x.GetUserByLogin(_fakeUser.Login), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            var model = result.Model as FeedbackModel;
            Assert.AreEqual(_fakeUser.Login, model.Login);
            Assert.AreEqual(_fakeUser.Email, model.Email);
        }

        [TestMethod]
        public void Test_Feedback_HttpPost()
        {
            var postModel = new FeedbackModel
            {
                Login = _fakeUser.Login,
                Email = _fakeUser.Email,
                Message = "Valid"
            };
            _controller.ModelState.Clear();
            var actionResult = _controller.Feedback(postModel);
            _mockUsersManager.Verify(x => x.InsertUserFeedback(_fakeUser.Login, _fakeUser.Email, "Valid"), Times.Once);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Panel", result.RouteValues["action"]);
            Assert.AreEqual("UserPanel", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_Feedback_HttpPost_InValid()
        {
            var postModel = new FeedbackModel
            {
                Login = _fakeUser.Login,
                Email = _fakeUser.Email,
                Message = "Valid"
            };
            _controller.ModelState.AddModelError("test", "test");
            var actionResult = _controller.Feedback(postModel);
            _mockUsersManager.Verify(x => x.InsertUserFeedback(_fakeUser.Login, _fakeUser.Email, "Valid"), Times.Never);
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.AreEqual("Feedback", result.ViewName);
            Assert.IsNotNull(result.Model);
            var newModel = result.Model as FeedbackModel;
            Assert.AreEqual(_fakeUser.Login, newModel.Login);
            Assert.AreEqual(_fakeUser.Email, newModel.Email);
            Assert.AreEqual("Valid", newModel.Message);
        }

        [TestMethod]
        public void Test_Reservations()
        {
            var actionResult = _controller.Reservations();
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsNull(result.View);
            var model = result.Model as StaticPagedList<HotelReservationDetailsModel>;
            Assert.AreEqual(_fakeHotelReservations[0].HotelId, model[0].ReservationDetails.HotelId);
            Assert.AreEqual(_fakeHotelReservations[0].HotelName, model[0].ReservationDetails.HotelName);
            Assert.AreEqual(_fakeHotelReservations[1].HotelId, model[1].ReservationDetails.HotelId);
            Assert.AreEqual(_fakeHotelReservations[1].HotelName, model[1].ReservationDetails.HotelName);
        }

        public void Test_Reservations_InvalidPage_1()
        {
            var actionResult = _controller.Reservations(page: 10);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Reservations", result.RouteValues["action"]);
            Assert.AreEqual((int)Math.Ceiling((double)TOTAL_HOTEL_RESERVATIONS / HOTEL_RESERVATIONS_PAGE_SIZE), result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_Reservations_InvalidPage_2()
        {
            var actionResult = _controller.Reservations(page: -1);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("Reservations", result.RouteValues["action"]);
            Assert.IsNull(result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_MyExcursions()
        {
            var actionResult = _controller.MyExcursions();
            Assert.IsInstanceOfType(actionResult, typeof(ViewResult));
            var result = actionResult as ViewResult;
            Assert.IsNotNull(result.Model);
            Assert.IsNull(result.View);
            var model = result.Model as StaticPagedList<ExcursionReservationDetailsModel>;
            Assert.AreEqual(_fakeExcursionReservation[0].UserId, model[0].ReservationDetails.UserId);
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionId, model[0].ReservationDetails.ExcursionId);
            Assert.AreEqual(_fakeExcursionReservation[0].ExcursionName, model[0].ReservationDetails.ExcursionName);
            Assert.AreEqual(_fakeExcursionReservation[1].UserId, model[1].ReservationDetails.UserId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionId, model[1].ReservationDetails.ExcursionId);
            Assert.AreEqual(_fakeExcursionReservation[1].ExcursionName, model[1].ReservationDetails.ExcursionName);
        }

        public void Test_MyExcursions_InvalidPage_1()
        {
            var actionResult = _controller.MyExcursions(page: 10);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("MyExcursions", result.RouteValues["action"]);
            Assert.AreEqual((int)Math.Ceiling((double)TOTAL_EXCURSION_RESERVATIONS / HOTEL_EXCURSION_PAGE_SIZE), result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        [TestMethod]
        public void Test_MyExcursions_InvalidPage_2()
        {
            var actionResult = _controller.MyExcursions(page: -1);
            Assert.IsInstanceOfType(actionResult, typeof(RedirectToRouteResult));
            var result = actionResult as RedirectToRouteResult;
            Assert.AreEqual("MyExcursions", result.RouteValues["action"]);
            Assert.IsNull(result.RouteValues["page"]);
            Assert.IsNull(result.RouteValues["controller"]);
        }

        #endregion

        #region Validation

        [TestMethod]
        public void Test_IsOldPasswordValid()
        {
            var pass = "pass";
            var result = _controller.IsOldPasswordValid(pass);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsOldPasswordValid_False()
        {
            var pass = "123";
            var result = _controller.IsOldPasswordValid(pass);
            Assert.AreEqual(false, result.Data);
        }

        [TestMethod]
        public void Test_IsNewPasswordValid()
        {
            var newPass = "pass";
            var oldPass = "passs";
            var result = _controller.IsNewPasswordValid(newPass, oldPass);
            Assert.AreEqual(true, result.Data);
        }

        [TestMethod]
        public void Test_IsNewPasswordValid_False()
        {
            var newPass = "pass";
            var oldPass = "pass";
            var result = _controller.IsNewPasswordValid(newPass, oldPass);
            Assert.AreEqual(false, result.Data);
        }

        #endregion
    }
}
