﻿using CityCenter.Entities;

namespace CityCenter.WebUI.Abstract
{
    public interface ISecurityManager
    {
        string GetCurrentUser();

        int GetCurrentUserId();

        bool Authentication(string login, string password);

        bool IsAuthenticated();

        bool IsUserInRole(string role);

        bool IsUserAdministratorOfHotel(int userId, int hotelId);

        bool IsUserAdministratorOfExcursion(int userId, int excursionId);

        string ReadUserRole(string login, out bool status);

        void SignOut();

        int RegisterNewUser(User user);

        void UpdateUserPassword(User user);

        bool CheckPassword(string hashedPass, string enteredPass);

        void PasswordRecovery(User user);
    }
}
