USE [CityCenter]

GO


CREATE PROC spBookRoom(
@UserId int,
@HotelRoomsId int,
@BeginDate date,
@EndDate date,
@Parking int,
@Pool int,
@Sauna int,
@TennisCourt int,
@Gym int,
@Status int,
@Id int OUTPUT

)
AS

BEGIN
IF NOT EXISTS (SELECT * FROM tblHotelRooms WHERE Id = @HotelRoomsId)
	RAISERROR ('Room type does not exist.', 16, 1);
IF (dbo.[IsRoomAvailable](@HotelRoomsId, @BeginDate, @EndDate) = 1)
BEGIN
	INSERT INTO tblHotelReservation ([UserId], [HotelRoomsId], [BeginDate], [EndDate], [Parking], [Pool], [Sauna], [TennisCourt], [Gym], [Status])
	VALUES (@UserId, @HotelRoomsId, @BeginDate, @EndDate, @Parking, @Pool, @Sauna, @TennisCourt, @Gym, @Status); 
	set @Id = SCOPE_IDENTITY();
END
ELSE 
	RAISERROR ('All rooms are booked!', 16, 1);
	set @Id = 0;
END


GO

CREATE PROC spUpdateRoles (
	@UserId int,
	@SA bit,
	@HA bit,
	@CA bit,
	@EA bit,
	@User bit
)

AS
	DECLARE @User_ID AS INT
	SET @User_ID = (SELECT Id FROM tblRole WHERE Name = 'User')
	DECLARE @SA_ID AS INT
	SET @SA_ID = (SELECT Id FROM tblRole WHERE Name = 'Admin')
	DECLARE @HA_ID AS INT
	SET @HA_ID = (SELECT Id FROM tblRole WHERE Name = 'HotelAdmin')
	DECLARE @CA_ID AS INT
	SET @CA_ID = (SELECT Id FROM tblRole WHERE Name = 'CafeAdmin')
	DECLARE @EA_ID AS INT
	SET @EA_ID = (SELECT Id FROM tblRole WHERE Name = 'ExcursionAdmin')
BEGIN
BEGIN TRANSACTION UpdateRoles

	DELETE FROM tblUserRoles 
	WHERE UserId = @UserId

	IF (@SA = 1)
	BEGIN
		INSERT INTO tblUserRoles (UserId, RoleId)
		VALUES (@UserId, @SA_ID)
	END
	IF (@HA = 1)
	BEGIN
		INSERT INTO tblUserRoles (UserId, RoleId)
		VALUES (@UserId, @HA_ID)
	END
	IF (@CA = 1)
	BEGIN
		INSERT INTO tblUserRoles (UserId, RoleId)
		VALUES (@UserId, @CA_ID)
	END
	IF (@EA = 1)
	BEGIN
		INSERT INTO tblUserRoles (UserId, RoleId)
		VALUES (@UserId, @EA_ID)
	END
	IF (@User = 1)
	BEGIN
		INSERT INTO tblUserRoles (UserId, RoleId)
		VALUES (@UserId, @User_ID)
	END

	COMMIT TRANSACTION UpdateRoles
END
