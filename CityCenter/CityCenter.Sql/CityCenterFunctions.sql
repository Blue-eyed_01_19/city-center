USE [CityCenter]

GO

CREATE FUNCTION IsRoomAvailable(@HotelRoomsId int, @BeginDate date, @EndDate date)
returns bit
AS
BEGIN
	declare @maxRoomsCount as int;
	declare @bookedRoomsCount as int;
	set @maxRoomsCount = (SELECT RoomsCount FROM tblHotelRooms WHERE Id = @HotelRoomsId);
	set @bookedRoomsCount = (SELECT COUNT(*) FROM tblHotelReservation WHERE HotelRoomsId = @HotelRoomsId AND BeginDate <= @EndDate AND EndDate >= @BeginDate);
	
	IF (@maxRoomsCount - @bookedRoomsCount > 0)
		return 1;
	return 0;
END
