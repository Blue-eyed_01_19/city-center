﻿using System.Collections.Generic;
using CityCenter.Entities;

namespace CityCenter.Repositories.Abstract
{
    public interface IHotelRepository
    {
        int BookRoom(int userId, int hotelRoomsId, string beginDate, string endDate, bool parking, bool pool, bool sauna, bool tennisCourt, bool gym, int status);

        List<HotelRooms> GetHotelRoomTypes(int hotelId);

        List<HotelRooms> GetAvailableHotelRoomTypes(int hotelId, string beginDate, string endDate);

        int GetHotelRoomsId(int hotelId, string roomType);

        List<Hotel> GetHotels();

        Hotel GetHotelById(int id);

        void UpdateHotelDescription(HotelDescription hotelDescription);

        List<HotelDescription> GetHotelDescriptions();

        HotelDescription GetHotelDescriptionById(int hotelId);

        List<HotelDescription> GetHotelDescriptions(int pageNumber, int pageSize);

        int GetHotelDescriptionsCount();

        int GetReservationCount(int userId);

        List<HotelReservationDetails> GetHotelReservationsByUserId(int userId);

        List<HotelReservationDetails> GetHotelReservationsByUserId(int userId, int pageNumber, int pageSize);

        void UpdateHotelRoom(HotelRooms room);

        int InsertHotelRoom(HotelRooms room);

        void DeletetHotelRoom(HotelRooms room);

        List<Hotel> GetHotelsByAdminId(int adminId);

        List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId);

        List<HotelReservationDetails> GetHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize, int status);

        Hotel GetHotelByHotelName(string hotelName);

        void UpdateHotelName(string name, int id);

        void UpdateHotelImage(HotelDescription hotelDescription);

        int GetReservationCountByHotelId(int hotelId, int status);

        void ChangeReservationStatus(int reservationId, int status);

        int GetPendingReservationCountByHotelId(int hotelId, int status);

        List<HotelReservationDetails> GetPendingHotelReservationByHotelId(int hotelId, int pageNumber, int pageSize, int status);

        int InsertHotel(Hotel hotel);

        void InsertNewHotelAdmin(int hotelId, int userId);

        void InsertHotelDescription(HotelDescription description);

        List<HotelReservationDetails> GetTodayPendingReservations();
    }
}
