﻿using System;
using System.Collections;
using System.Collections.Generic;
using CityCenter.Entities;

namespace CityCenter.Repositories.Abstract
{
    public interface IExcursionsRepository
    {
        int InsertReservation(int userId, int excursionId, DateTime excursionDate, int status);

        List<Excursion> GetExcursions();

        List<Excursion> GetExcursions(int pageNumber, int pageSize);

        List<ExcursionDescription> GetExcursionDescriptions();

        List<ExcursionDescription> GetExcursionDescriptions(int pageNumber, int pageSize);

        int GetExcursionDescriptionsCount();

        Excursion GetExcursionById(int id);

        Excursion GetExcursionByName(string name);

        ExcursionDescription GetExcursionDescriptionById(int excursionId);

        int GetReservationDetailsCountByUserId(int userId);

        List<ExcursionReservationDetails> GetReservationDetailsByUserId(int userId, int pageNumber, int pageSize);

        int GetAcceptedReservationsCountByExcursionId(int excursionId);

        List<ExcursionReservationDetails> GetAcceptedReservationsByExcursionId(int excursionId, int pageNumber, int pageSize);

        int GetPendingReservationsCountByExcursionId(int excursionId);

        List<ExcursionReservationDetails> GetPendingReservationsByExcursionId(int excursionId, int pageNumber, int pageSize);

        void UpdateExcursionImage(ExcursionDescription excursionDescription);

        void UpdateExcursionDescription(ExcursionDescription excursionDescription);

        void UpdateExcursionName(string name, int id);

        void ChangeReservationStatus(int reservationId, int status);

        List<Excursion> GetExcursionsByAdminId(int adminId);

        int InsertExcursion(Excursion excursion);

        void InsertExcursionDescription(ExcursionDescription excursion);

        void InsertNewExcursionAdmin(int excursionId, int userId);

        List<ExcursionReservationDetails> GetTodayPendingReservations();
    }
}
