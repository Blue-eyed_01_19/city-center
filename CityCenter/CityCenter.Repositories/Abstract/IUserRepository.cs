﻿using System;
using System.Collections.Generic;
using CityCenter.Entities;

namespace CityCenter.Repositories.Abstract
{
    public interface IUserRepository
    {
        List<User> GetUsers();

        User GetUserById(int id);

        User GetUserByLogin(string login);

        User GetUserByEmail(string email);

        User GetUserByPhone(string phone);

        string GetUserRegistrationToken(int userId);

        int InsertUser(User user);

        void DeleteUser(User user);

        void UpdateUser(User user);

        void UpdateUserPassword(User user);

        int InsertUnconfirmedRegistration(int userId, string token, DateTime date);

        int InserPasswordRecoveryRequest(int userId, string token, DateTime date);

        List<Role> GetRoles();

        Role GetRoleByName(string name);

        List<Role> GetUserRoles(int userId);

        List<User> GetUsersByRole(int roleId);

        List<User> GetUsersByTextFilter(string text);
        
        void SetRoles(int userId, bool isUser = false, bool isAdmin = false, bool isHotelAdmin = false, bool isCafeAdmin = false, bool isExcursionAdmin = false);

        void UpdateUserStatus(int userId, bool status);

        User GetUser(string login, string password);

        void DeleteUnconfirmRegistration(int userId);

        void InsertUserFeedback(string login, string email, string message);

        int IsUserAdministratorOfHotel(int userId, int hotelId);

        int IsUserAdministratorOfExcursion(int userId, int hotelId);

        int GetUserIdByReservationId(int id);

        string GetUserPasswordRecoveryToken(int userId);

        void DeletePasswordRecoveryToken(int userId);
    }
}
