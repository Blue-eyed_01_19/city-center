﻿using System.Configuration;
using System.Web;
using System.Web.Mvc;
using CityCenter.Repositories.Sql;
using CityCenter.WebUI.Abstract;

namespace CityCenter.WebUI.Frontend.Code
{
    public class AuthorizeOrThrow404Attribute : AuthorizeAttribute
    {
        private readonly ISecurityManager _securityManager = new SecurityManager(new UserRepository(
                                                             ConfigurationManager.ConnectionStrings["CityCenter"].ConnectionString));
        private readonly string[] _allowedroles;

        public AuthorizeOrThrow404Attribute(params string[] roles)
        {
            _allowedroles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorize = false;
            foreach (var role in _allowedroles)
            {
                if (_securityManager.IsUserInRole(role))
                {
                    authorize = true;
                }
            }
            if (!authorize)
            {
                throw new HttpException(404, "HTTP/1.1 404 Not Found");
            }
            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}