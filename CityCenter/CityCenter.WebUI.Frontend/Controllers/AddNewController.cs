﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Code;
using CityCenter.WebUI.Frontend.Models.Excursion;
using CityCenter.WebUI.Frontend.Models.Hotel;

namespace CityCenter.WebUI.Frontend.Controllers
{
    
    public class AddNewController : Controller
    {
        #region Fields

        private readonly IExcursionsManager _excursionsManager;
        private readonly ISecurityManager _securityManager;
        private readonly IHotelsManager _hotelsManager;

        #endregion

        #region Constructor

        public AddNewController(IExcursionsManager excursionsManager, ISecurityManager securityManager, IHotelsManager hotelsManager)
        {
            _excursionsManager = excursionsManager;
            _securityManager = securityManager;
            _hotelsManager = hotelsManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        [AuthorizeOrThrow404("ExcursionAdmin")]
        public ActionResult AddNewExcursion()
        {
            return View("NewExcursion");
        }

        [HttpPost]
        [AuthorizeOrThrow404("ExcursionAdmin")]
        public ActionResult AddNewExcursion(NewExcursionModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = _securityManager.GetCurrentUserId();
                var newExcursionId = _excursionsManager.AddNewExcursion(
                    new Excursion()
                    {
                        Address = model.Address,
                        BeginTime = TimeSpan.Parse(model.BeginTime),
                        Duration = TimeSpan.Parse(model.Duration),
                        Name = model.Name,
                        Price = model.Price
                    },
                    new ExcursionDescription()
                    {
                        LongDescription = model.LongDescription,
                        ShortDescription = model.ShortDescription,
                        Photo = UploadDefaultImage(),
                        PhotoExtension = "image/jpeg"
                    },
                    userId);
                return RedirectToAction("EditExcursionContent", "ExcursionAdministratorPanel", new { excursionId = newExcursionId });
            }
            return View("NewExcursion", model);
        }

        [HttpGet]
        [AuthorizeOrThrow404("HotelAdmin")]
        public ActionResult AddNewHotel()
        {
            return View("NewHotel");
        }

        [HttpPost]
        [AuthorizeOrThrow404("HotelAdmin")]
        public ActionResult AddNewHotel(AddNewHotelModel model)
        {
            if (ModelState.IsValid)
            {
                var rooms = GetHotelRooms(model);
                var userId = _securityManager.GetCurrentUserId();
                var newHotelId = _hotelsManager.AddNewHotel(
                    userId,
                    new Hotel()
                    {
                        Name = model.Name,
                        Address = model.Address,
                        Phones = model.Phone,
                        Rating = model.Rating,
                        Parking = model.Parking,
                        Pool = model.Pool,
                        Sauna = model.Sauna,
                        TennisCourt = model.TennisCourt,
                        Gym = model.Gym
                    },
                    rooms,
                    new HotelDescription()
                    {
                        Header = model.Header,
                        Description = model.Description,
                        Photo = UploadDefaultImage(),
                        PhotoExtension = "image/jpeg",
                        Phone = model.Phone,
                        Address = model.Address,
                        Site = model.Site
                    }
                    );
                return RedirectToAction("EditHotelContent", "HotelAdministratorPanel", new { hotelId = newHotelId });
            }
            return View("NewHotel", model);
        }

        #endregion

        #region Validation

        public JsonResult IsDurationValid(string duration)
        {
            if (duration.Length > 5 || duration.Length < 3)
            {
                return Json("Ivalid duration format( HH:MM)", JsonRequestBehavior.AllowGet);
            }
            TimeSpan res;
            return Json(TimeSpan.TryParse(duration, out res), JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsBeginTimeValid(string begintime)
        {
            if (begintime.Length > 5 || begintime.Length < 3)
            {
                return Json("Ivalid begintime format( HH:MM)", JsonRequestBehavior.AllowGet);
            }
            TimeSpan res;
            return Json(TimeSpan.TryParse(begintime, out res), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Helpers

        private byte[] UploadDefaultImage()
        {
            var image = Image.FromFile(Server.MapPath("~/Content/images/excursions-1.jpg"));
            var ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private bool IsRoomValid(HotelRooms room)
        {
            return room.RoomType != null && room.Price > 0 && room.RoomsCount > 0;
        }

        private List<HotelRooms> GetHotelRooms(AddNewHotelModel model)
        {
            var room1 = new HotelRooms()
            {
                Price = model.Price1 ?? 0,
                RoomsCount = model.RoomsCount1 ?? 0,
                RoomType = model.RoomType1
            };
            var room2 = new HotelRooms()
            {
                Price = model.Price2 ?? 0,
                RoomsCount = model.RoomsCount2 ?? 0,
                RoomType = model.RoomType2
            };
            var room3 = new HotelRooms()
            {
                Price = model.Price3 ?? 0,
                RoomsCount = model.RoomsCount3 ?? 0,
                RoomType = model.RoomType3
            };
            var room4 = new HotelRooms()
            {
                Price = model.Price4 ?? 0,
                RoomsCount = model.RoomsCount4 ?? 0,
                RoomType = model.RoomType4
            };
            var rooms = new List<HotelRooms>() { room1, room2, room3, room4 }.Where(IsRoomValid).ToList();
            return rooms;
        }

        #endregion
    }
}