﻿using System;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Models.Excursion;

namespace CityCenter.WebUI.Frontend.Controllers
{
    [Authorize]
    public class ExcursionRegistrationController : Controller
    {

        #region Fields

        private readonly ISecurityManager _securityManager;
        private readonly IUsersManager _usersManager;
        private readonly IExcursionsManager _excursionsManager;

        #endregion

        #region Constructor

        public ExcursionRegistrationController(ISecurityManager securityManager, IUsersManager usersManager, IExcursionsManager excursionsManager)
        {
            _securityManager = securityManager;
            _usersManager = usersManager;
            _excursionsManager = excursionsManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult ExcursionRegistration(int excursionId)
        {
            var userId = _securityManager.GetCurrentUserId();
            var user = _usersManager.GetUserById(userId);
            var model = new ExcursionRegistrationModel()
            {
                FirstName = user.FirstName,
                Surname = user.Surname,
                Email = user.Email,
                Phone = user.Phone,
                ExcursionId = excursionId
            };
            ViewBag.Name = _excursionsManager.GetExcursionById(excursionId).Name;
            return View(model);
        }

        [HttpPost]
        public ActionResult ExcursionRegistration(ExcursionRegistrationModel model)
        {          
            if (ModelState.IsValid)
            {
                var username = _securityManager.GetCurrentUser();
                var userId = _usersManager.GetUserByLogin(username).Id;
                _excursionsManager.AddNewReservation(userId, model.ExcursionId, DateTime.Parse(model.Date));

                return View("RequetSend");
            }
            return View("ExcursionRegistration", model);
        }

        #endregion

        #region Validation

        [HttpGet]
        public JsonResult IsDateValid(string date)
        {
            DateTime res;
            return Json(DateTime.TryParse(date, out res), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}