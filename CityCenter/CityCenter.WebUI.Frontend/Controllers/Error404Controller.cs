﻿using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class Error404Controller : Controller
    {
        [HttpGet]
        public ActionResult Error404()
        {
            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;
            return View();
        }
    }
}