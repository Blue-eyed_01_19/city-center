﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Frontend.Models.Helper;
using CityCenter.WebUI.Frontend.Models.Hotel;
using PagedList;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class HotelsController : Controller
    {
        #region Fields

        private readonly IHotelsManager _hotelsManager;

        #endregion

        #region Constructor

        public HotelsController(IHotelsManager hotelsManager)
        {
            _hotelsManager = hotelsManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Hotels(int page = 1)
        {
            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 1,
                TotalItems = _hotelsManager.GetHotelDescriptionsCount()
            };

            #region Validation

            if (page < 1)
            {
                return RedirectToAction("Hotels");
            }
            if ((page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("Hotels", new { page = (int)Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize) });
            }

            #endregion

            var hotelEntities = _hotelsManager.GetHotelDescriptions(pagedListModel.Page, pagedListModel.PageSize);
            var hotels = hotelEntities.Select(item => new HotelDescriptionModel()
            {
                HotelDescription = item,
                HotelName = _hotelsManager.GetHotelById(item.HotelId).Name
            }).ToList();

            var pagedList = new StaticPagedList<HotelDescriptionModel>(hotels, page, pagedListModel.PageSize, pagedListModel.TotalItems);

            return View(pagedList);
        }

        [HttpGet]
        public FileContentResult GetHotelImage(int hotelId)
        {
            var desc = _hotelsManager.GetHotelDescriptionById(hotelId);
            return File(desc.Photo, MimeMapping.GetMimeMapping(desc.PhotoExtension));
        }

        #endregion

    }
}
