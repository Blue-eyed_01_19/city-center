﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Models.Hotel;

namespace CityCenter.WebUI.Frontend.Controllers
{
    public class HotelBookingController : Controller
    {
        #region Fields

        private readonly IUsersManager _usersManager;
        private readonly IHotelsManager _hotelsManager;
        private readonly ISecurityManager _securityManager;

        #endregion

        #region Constructor

        public HotelBookingController(IUsersManager usersManager, IHotelsManager hotelsManager, ISecurityManager securityManager)
        {
            _usersManager = usersManager;
            _hotelsManager = hotelsManager;
            _securityManager = securityManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        [Authorize]
        public ActionResult HotelBooking(int hotelId)
        {
            var hotel = _hotelsManager.GetHotelById(hotelId);
             
            #region Validation

            if (hotel == null)
            {
                return RedirectToAction("Hotels", "Hotels");
            }
            
            #endregion
            
            var username = _securityManager.GetCurrentUser();
            var user = _usersManager.GetUserByLogin(username);
            var model = new BookingRoomModel
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                Surname = user.Surname,
                Email = user.Email,
                Phone = user.Phone,
                HotelId = hotelId,
                Parking = hotel.Parking,
                Pool = hotel.Pool,
                Sauna = hotel.Sauna,
                TennisCourt = hotel.TennisCourt,
                Gym = hotel.Gym,
                RoomType = ConvertToSelectListItem(_hotelsManager.GetHotelRoomTypes(hotelId))
            };
            ViewBag.HotelName = hotel.Name;
            return View(model);
        }

        [HttpPost]
        public ActionResult HotelBookingSubmit(BookingRoomModel model)
        {
            var username = _securityManager.GetCurrentUser();
            var userId = _usersManager.GetUserByLogin(username).Id;
            if (ModelState.IsValid)
            {
                _hotelsManager.BookRoom(userId, model.SelectedRoomType, DateTime.Parse(model.BeginDate), DateTime.Parse(model.EndDate), model.Parking, model.Pool, model.Sauna, model.TennisCourt, model.Gym);

                return View("RequetSend");
            }
            model.RoomType = ConvertToSelectListItem(_hotelsManager.GetHotelRoomTypes(model.HotelId));
            return View("HotelBooking", model);
        }

        [HttpPost]
        public ActionResult GetAvailableRoomTypes(int hotelId, DateTime beginDate, DateTime endDate)
        {
            var availableRoomTypes = _hotelsManager.GetAvailableHotelRoomTypes(hotelId, beginDate, endDate);
            if (endDate != beginDate)
            {
                var availableRoomTypeIds = availableRoomTypes.Select(r => r.Id).ToArray();
                return Json(new
                {
                    availableRoomTypeIds
                });
            }
            return Json("Minimal booking period : 1 day", JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Validation

        [HttpGet]
        public JsonResult IsBeginDateValid(string beginDate)
        {
            DateTime res;
            return Json(DateTime.TryParse(beginDate, out res), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsEndDateValid(string endDate, string beginDate)
        {
            DateTime end;
            DateTime begin;
            if (!DateTime.TryParse(endDate, out end) || !DateTime.TryParse(beginDate, out begin))
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return (end - begin).Days < 1
                ? Json("Minimal booking period : 1 day", JsonRequestBehavior.AllowGet)
                : Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Helper

        private static List<SelectListItem> ConvertToSelectListItem(List<HotelRooms> types)
        {
            return types.Select(type => new SelectListItem() 
                            { 
                                Text = string.Format(new CultureInfo("uk-UA", false), "{0} - {1:C}", type.RoomType, type.Price), 
                                Value = type.Id.ToString() 
                            }).ToList();
        }

        #endregion
    }
}
