﻿using System;
using System.Linq;
using System.Web.Mvc;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Frontend.Models.Account;
using CityCenter.WebUI.Frontend.Models.Helper;
using CityCenter.WebUI.Frontend.Models.Panel;
using PagedList;

namespace CityCenter.WebUI.Frontend.Controllers
{
    [Authorize]
    public class UserPanelController : Controller
    {
        #region Fields

        private readonly IUsersManager _usersManager;
        private readonly ISecurityManager _securityManager;
        private readonly IHotelsManager _hotelsManager;
        private readonly IExcursionsManager _excursionsManager;

        #endregion

        #region Constructor

        public UserPanelController(IUsersManager usersManager, ISecurityManager securityManager, IHotelsManager hotelManager, IExcursionsManager excursionsManager)
        {
            _usersManager = usersManager;
            _securityManager = securityManager;
            _hotelsManager = hotelManager;
            _excursionsManager = excursionsManager;
        }

        #endregion

        #region WebActions

        [HttpGet]
        public ActionResult Panel()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Info()
        {
            var username = _securityManager.GetCurrentUser();
            var user = _usersManager.GetUserByLogin(username);
            var model = new PanelUserInfoModel()
            {
                FirstName = user.FirstName,
                Surname = user.Surname,
                Email = user.Email,
                Phone = user.Phone,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Info(PanelUserInfoModel model)
        {
            if (ModelState.IsValid)
            {
                var username = _securityManager.GetCurrentUser();
                var user = _usersManager.GetUserByLogin(username);
                user.FirstName = model.FirstName;
                user.Surname = model.Surname;
                user.Email = model.Email;
                user.Phone = model.Phone;
                _usersManager.UpdateUser(user);
                return RedirectToAction("Panel", "UserPanel");
            }
            return View("Info", model);
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(СhangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var username = _securityManager.GetCurrentUser();
                var user = _usersManager.GetUserByLogin(username);
                user.Password = model.NewPassword;
                _securityManager.UpdateUserPassword(user);
                return RedirectToAction("Panel", "UserPanel");
            }
            return View("ChangePassword");
        }

        [HttpGet]
        public ActionResult Feedback()
        {
            var username = _securityManager.GetCurrentUser();
            var user = _usersManager.GetUserByLogin(username);
            var model = new FeedbackModel()
            {
                Login = user.Login,
                Email = user.Email,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Feedback(FeedbackModel model)
        {
            if (ModelState.IsValid)
            {
                _usersManager.InsertUserFeedback(model.Login, model.Email, model.Message);
                return RedirectToAction("Panel", "UserPanel");
            }
            return View("Feedback", model);
        }

        [HttpGet]
        public ActionResult Reservations(int page = 1)
        {
            var userId = _securityManager.GetCurrentUserId();
            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 3,
                TotalItems = _hotelsManager.GetReservationCount(userId)
            };

            ViewBag.TotalItems = pagedListModel.TotalItems;
            ViewBag.PageSize = pagedListModel.PageSize;

            #region Paging Validation

            if (page < 1)
            {
                return RedirectToAction("Reservations");
            }
            if (page != 1 && (page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("Reservations", new { page = (int)Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize) });
            }

            #endregion

            var reservationEntities = _hotelsManager.GetHotelReservationDetails(userId, pagedListModel.Page, pagedListModel.PageSize);

            var reservations = reservationEntities.Select(item => new HotelReservationDetailsModel()
            {
                ReservationDetails = item

            }).ToList();

            var pagedList = new StaticPagedList<HotelReservationDetailsModel>(reservations, page, pagedListModel.PageSize,
                pagedListModel.TotalItems);

            return View(pagedList);
        }

        [HttpGet]
        public ActionResult MyExcursions(int page = 1)
        {
            var userId = _securityManager.GetCurrentUserId();
            var pagedListModel = new PagedListModel()
            {
                Page = page,
                PageSize = 3,
                TotalItems = _excursionsManager.GetReservationDetailsCountByUserId(userId)
            };

            ViewBag.TotalItems = pagedListModel.TotalItems;
            ViewBag.PageSize = pagedListModel.PageSize;

            #region Paging Validation

            if (page < 1)
            {
                return RedirectToAction("MyExcursions");
            }
            if (page != 1 && (page - 1) * pagedListModel.PageSize >= pagedListModel.TotalItems)
            {
                return RedirectToAction("MyExcursions", new { page = (int)Math.Ceiling((double)pagedListModel.TotalItems / pagedListModel.PageSize) });
            }

            #endregion

            var reservationEntities = _excursionsManager.GetReservationDetailsByUserId(userId, pagedListModel.Page,
               pagedListModel.PageSize);

            var reservations = reservationEntities.Select(item => new ExcursionReservationDetailsModel()
            {
                ReservationDetails = item,
                User = _usersManager.GetUserById(item.UserId)
            }).ToList();

            var pagedList = new StaticPagedList<ExcursionReservationDetailsModel>(reservations, page, pagedListModel.PageSize,
                pagedListModel.TotalItems);

            return View(pagedList);
        }

        #endregion

        #region Validation

        [HttpGet]
        public JsonResult IsOldPasswordValid(string oldPassword)
        {
            var username = _securityManager.GetCurrentUser();
            var user = _usersManager.GetUserByLogin(username);
            return Json(_securityManager.CheckPassword(user.Password, oldPassword), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsNewPasswordValid(string newPassword, string oldPassword)
        {
            return Json(newPassword != oldPassword, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}