﻿using System.Web.Optimization;

namespace CityCenter.WebUI.Frontend
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/libs/jquery/jquery-2.1.4.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/hotel-reserve").Include(
                        "~/Scripts/pages/hotel-reserve-page.js"));

            bundles.Add(new ScriptBundle("~/bundles/datapicker").Include(
                        "~/Scripts/libs/jquery/datapicker/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-validation").Include(
                        "~/Scripts/libs/jquery/validation/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/add-new-hotel").Include(
                        "~/Scripts/pages/add-new-hotel.js"));
            
            bundles.Add(new StyleBundle("~/Content/datapicker").Include(
                      "~/Content/libs/datapicker/jquery-ui.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/account").Include(
                      "~/Content/styles/sign-up.css").Include(
                      "~/Content/styles/style-for-excursions.css").Include(
                       "~/Content/styles/excursions.css"));

            bundles.Add(new StyleBundle("~/Content/confirm").Include(
                      "~/Content//styles/confirm.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/hotel-reserve").Include(
                      "~/Content/styles/request-form.css").Include(
                      "~/Content/styles/style-for-excursions.css").Include(
                       "~/Content/styles/excursions.css"));

            bundles.Add(new StyleBundle("~/Content/hotels").Include(
                      "~/Content//styles/hotels.css", new CssRewriteUrlTransform()).Include(
                      "~/Content/styles/paged-list.css"));

            bundles.Add(new StyleBundle("~/Content/main").Include(
                      "~/Content//styles/style.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/paged-list").Include(
                      "~/Content/styles/paged-list.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/edit-content").Include(
                      "~/Content/styles/edit-hotel-content.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/user-info").Include(
                      "~/Content/styles/user-info.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/show-reservations").Include(
                      "~/Content/styles/reservation.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/error-404").Include(
                      "~/Content/styles/error-404.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/error-500").Include(
                      "~/Content/styles/error-500.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/excursion").Include(
                      "~/Content/styles/excursions.css", new CssRewriteUrlTransform()).Include(
                      "~/Content/styles/style-for-excursions.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
