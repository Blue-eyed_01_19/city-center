using System.Configuration;
using System.Web.Mvc;
using CityCenter.BLL;
using CityCenter.BLL.Abstract;
using CityCenter.Repositories.Abstract;
using CityCenter.Repositories.Sql;
using CityCenter.WebUI.Abstract;
using Microsoft.Practices.Unity;
using NLog;
using Unity.Mvc5;

namespace CityCenter.WebUI.Frontend
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            var connstr = ConfigurationManager.ConnectionStrings["CityCenter"].ConnectionString;

            container.RegisterType<IUsersManager, UsersManager>();
            container.RegisterType<IHotelsManager, HotelsManager>();
            container.RegisterType<IUserRepository, UserRepository>(new InjectionConstructor(connstr));
            container.RegisterType<IHotelRepository, HotelRepository>(new InjectionConstructor(connstr));
            container.RegisterType<ISecurityManager, SecurityManager>();
            container.RegisterType<IExcursionsRepository, ExcursionRepository>(new InjectionConstructor(connstr));
            container.RegisterType<IExcursionsManager, ExcursionsManager>();
            container.RegisterType<IEmailManager, EmailManager>();
            container.RegisterType<IAutoDeclineService, AutoDeclineService>();
            container.RegisterType<ILogger, Logger>(new InjectionFactory(f => LogManager.GetCurrentClassLogger()));

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}