﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Models.Hotel
{
    public class BookingRoomModel
    {
        public int UserId { get; set; }

        [DisplayName("First Name")]
        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid first name")]
        public string FirstName { get; set; }

        [DisplayName("Surname")]
        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid surname")]
        public string Surname { get; set; }

        [DisplayName("Email")]
        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid email")]
        [Editable(true)]
        public string Email { get; set; }

        [DisplayName("Phone")]
        [Required]
        [StringLength(13, ErrorMessage = "Length should be 13")]
        [RegularExpression(@"^\+[1-9]{1}[0-9]{11}$", ErrorMessage = "Invalid Phone")]
        [Editable(true)]
        public string Phone { get; set; }

        public IList<SelectListItem> RoomType { get; set; }

        [DisplayName("Room type")]
        [Required]
        public int SelectedRoomType { get; set; }

        public int HotelId { get; set; }

        public bool Parking { get; set; }

        public bool Pool { get; set; }

        public bool Sauna { get; set; }

        public bool TennisCourt { get; set; }

        public bool Gym { get; set; }

        [Required]
        [Remote("IsBeginDateValid", "HotelBooking", ErrorMessage = "Invalid begin date")]
        public string BeginDate { get; set; }

        [Required]
        [Remote("IsEndDateValid", "HotelBooking", ErrorMessage = "Invalid end date", AdditionalFields = "BeginDate")]
        public string EndDate { get; set; }

    }
}