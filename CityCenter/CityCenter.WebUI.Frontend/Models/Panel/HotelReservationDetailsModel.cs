﻿using CityCenter.Entities;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class HotelReservationDetailsModel
    {
        public HotelReservationDetails ReservationDetails { get; set; }
    }
}