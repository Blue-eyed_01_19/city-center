﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CityCenter.Entities;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class ExcursionAdministratorModel
    {
        public List<Role> UserRoles { get; set; }

        public IList<SelectListItem> CurrentExcursion { get; set; }

        [DisplayName("Excursion")]
        [Required]
        public int SelectedExcursionId { get; set; }
    }
}