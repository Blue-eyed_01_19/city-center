﻿using CityCenter.Entities;
using PagedList;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class AdminHotelReservationDetailsModel
    {
        public StaticPagedList<HotelReservationDetails> ReservationDetails { get; set; }

        public int HotelId { get; set; }
    }
}