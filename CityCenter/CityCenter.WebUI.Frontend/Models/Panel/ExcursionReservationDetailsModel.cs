﻿using CityCenter.Entities;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class ExcursionReservationDetailsModel
    {
        public ExcursionReservationDetails ReservationDetails { get; set; }

        public User User { get; set; }
    }
}