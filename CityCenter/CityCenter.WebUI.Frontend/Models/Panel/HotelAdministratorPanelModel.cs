﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CityCenter.Entities;

namespace CityCenter.WebUI.Frontend.Models.Panel
{
    public class HotelAdministratorPanelModel
    {
        public List<Role> UserRoles { get; set; }

        public IList<SelectListItem> CurrentHotels { get; set; }

        [DisplayName("Hotel")]
        [Required]
        public int SelectedHotelId { get; set; }
    }
}