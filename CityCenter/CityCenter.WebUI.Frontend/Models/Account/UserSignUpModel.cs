﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Models.Account
{
    public class UserSignUpModel
    {
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        [RegularExpression(@"^[0-9a-zA-Z]{1,20}", ErrorMessage = "Invalid login")]
        [Remote("IsLoginValid", "Account", ErrorMessage = "Login already exsist")]
        public string Login { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid first name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid surname")]
        public string Surname { get; set; }

        [Required]
        [StringLength(13, ErrorMessage = "Length should be 13")]
        [RegularExpression(@"^\+[1-9]{1}[0-9]{11}$", ErrorMessage = "Invalid Phone")]
        [Remote("IsPhoneValid", "Account", ErrorMessage = "Phone already exsist")]
        public string Phone { get; set; }
        
        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid email")]
        [Remote("IsEmailValid", "Account", ErrorMessage = "E-mail already exsist")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,30}$", ErrorMessage = "Password must have large and small letters, numbers. Min-Length: 8 symbol. Max-Length: 30 symbol.")]
        
        public string Password { get; set; }

        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string PasswordConfirmation { get; set; }
    }
}