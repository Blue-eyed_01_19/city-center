﻿using System.ComponentModel.DataAnnotations;

namespace CityCenter.WebUI.Frontend.Models.Account
{
    public class PasswordRecoveryModel
    {
        public int UserId { get; set; }

        [Required]
        [RegularExpression(@"^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,30}$", ErrorMessage = "Password must have large and small letters, numbers. Min-Length: 8 symbol. Max-Length: 30 symbol.")]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        public string NewPassword { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword")]
        public string NewPasswordConfirmation { get; set; }
    }
}