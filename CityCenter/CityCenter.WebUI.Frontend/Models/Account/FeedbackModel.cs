﻿using System.ComponentModel.DataAnnotations;

namespace CityCenter.WebUI.Frontend.Models.Account
{
    public class FeedbackModel
    {
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        [RegularExpression(@"^[0-9a-zA-Zа-яА-ЯіїІЇєЄёЁ]{1,20}", ErrorMessage = "Invalid name")]
        public string Login { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Cannot be required")]
        [StringLength(4000, ErrorMessage = "Length should not exceed 4000")]
        public string Message { get; set; }
    }
}