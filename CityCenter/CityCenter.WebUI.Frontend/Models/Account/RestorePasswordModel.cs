﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Models.Account
{
    public class RestorePasswordModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid email")]
        [Remote("IsEmailExist", "Account", ErrorMessage = "E-mail doesn't exsist")]
        public string Email { get; set; }
    }
}