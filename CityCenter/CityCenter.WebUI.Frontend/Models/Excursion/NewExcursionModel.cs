﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Models.Excursion
{
    public class NewExcursionModel
    {
        [Required]
        [StringLength(20, ErrorMessage = "Length should not exceed 20")]
        [RegularExpression(@"^[a-zA-Z,. ()-]{1,20}", ErrorMessage = "Invalid excursion name")]
        public string Name { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        public string Address { get; set; }

        [Required]
        [Remote("IsDurationValid", "AddNew", ErrorMessage = "Invalid duration, format --:--")]
        public string Duration { get; set; }

        [Required]
        [Remote("IsBeginTimeValid", "AddNew", ErrorMessage = "Invalid begin time, format --:--")]
        public string BeginTime { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        [StringLength(500, ErrorMessage = "Length should not exceed 500")]
        public string ShortDescription { get; set; }

        [Required]
        public string LongDescription { get; set; }

    }
}