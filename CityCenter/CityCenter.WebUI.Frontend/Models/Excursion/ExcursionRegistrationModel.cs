﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CityCenter.WebUI.Frontend.Models.Excursion
{
    public class ExcursionRegistrationModel
    {
        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid first name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Length should not exceed 30")]
        [RegularExpression(@"^[a-zA-Zа-яА-ЯіІ]{1,30}", ErrorMessage = "Invalid surname")]
        public string Surname { get; set; }

        [Required]
        [StringLength(13, ErrorMessage = "Length should be 13")]
        [RegularExpression(@"^\+[1-9]{1}[0-9]{11}$", ErrorMessage = "Invalid Phone")]
        public string Phone { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Length should not exceed 50")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid email")]
        public string Email { get; set; }

        public int ExcursionId { get; set; }

        [Required]
        [Remote("IsDateValid", "ExcursionRegistration", ErrorMessage = "Invalid begin date")]
        public string Date { get; set; }
    }
}