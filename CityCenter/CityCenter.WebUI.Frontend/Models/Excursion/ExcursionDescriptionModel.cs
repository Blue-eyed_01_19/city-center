﻿using CityCenter.Entities;

namespace CityCenter.WebUI.Frontend.Models.Excursion
{
    public class ExcursionDescriptionModel
    {
        public string Name { get; set; }

        public ExcursionDescription Description { get; set; }

        public Entities.Excursion Excursion { get; set; }
    }
}