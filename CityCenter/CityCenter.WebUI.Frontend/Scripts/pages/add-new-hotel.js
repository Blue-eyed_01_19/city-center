﻿(function ($) {
    var AddNewHotel = function ($rootElement) {
        var _self = this;

        var _private = {};

        _private.$rootElement = $rootElement;

        _private.initialize = function () {
            _private.$addOne = $rootElement.find("#add-one");
            _private.$twoHidden = $rootElement.find("#two");
            _private.$addOne.on("click", _private.addOne_Click);

            _private.$addTwo = $rootElement.find("#add-two");
            _private.$threeHidden = $rootElement.find("#three");
            _private.$addTwo.on("click", _private.addTwo_Click);

            _private.$addThree = $rootElement.find("#add-three");
            _private.$fourHidden = $rootElement.find("#four");
            _private.$addThree.on("click", _private.addThree_Click);

            _private.$checkbox = $rootElement.find(".checkbox");


            _private.checkboxUncheck();
        };

        _private.addOne_Click = function () {
            $(_private.$twoHidden).removeClass("hide");
            $(_private.$twoHidden).addClass("show");
        };

        _private.addTwo_Click = function () {
            $(_private.$threeHidden).removeClass("hide");
            $(_private.$threeHidden).addClass("show");
        };

        _private.addThree_Click = function () {
            $(_private.$fourHidden).removeClass("hide");
            $(_private.$fourHidden).addClass("show");
        };

        _private.checkboxUncheck = function () {
            $(_private.$checkbox).prop("checked", false);
        };
        _private.initialize();

    };

    $(document).on("ready", function () {
        var page = new AddNewHotel($("#request-form"));
    });

})(window.jQuery);