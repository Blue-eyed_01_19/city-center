﻿(function ($) {
    var ExcursionReserve = function ($rootElement) {
        var _self = this;

        var _private = {};

        _private.$rootElement = $rootElement;

        _private.initialize = function () {
            _private.$beginDate = $rootElement.find("#Date");

            _private.date();
        };

        _private.date = function () {
            $(_private.$beginDate).datepicker({
                minDate: "1",
                maxDate: "7",
                changeYear: true,
                changeMonth: true,
                dateFormat: "DD, d MM, yy"
            });
        };

        _private.initialize();

    };

    $(document).on("ready", function () {
        var page = new ExcursionReserve($("#request-form"));
    });

})(window.jQuery);