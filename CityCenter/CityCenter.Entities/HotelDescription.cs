﻿namespace CityCenter.Entities
{
    public class HotelDescription
    {

        public int HotelId { get; set; }

        public string Header { get; set; }

        public string Description { get; set; }

        public byte[] Photo { get; set; }

        public string PhotoExtension { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string Site { get; set; }
    }
}
