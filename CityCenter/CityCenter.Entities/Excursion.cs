﻿using System;

namespace CityCenter.Entities
{
    public class Excursion
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public TimeSpan Duration { get; set; }

        public TimeSpan BeginTime { get; set; }

        public decimal Price { get; set; }
    }
}
