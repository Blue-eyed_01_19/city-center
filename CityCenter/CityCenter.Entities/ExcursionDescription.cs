﻿namespace CityCenter.Entities
{
    public class ExcursionDescription
    {
        public int ExcursionId { get; set; }

        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        public byte[] Photo { get; set; }

        public string PhotoExtension { get; set; }
    }
}
