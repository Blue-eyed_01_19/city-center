﻿namespace CityCenter.Entities
{
    public class HotelRooms
    {
        public int Id { get; set; }

        public int HotelId { get; set; }

        public string RoomType { get; set; }

        public decimal Price { get; set; }

        public int RoomsCount { get; set; }
    }
}
