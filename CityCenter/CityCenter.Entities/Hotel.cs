﻿namespace CityCenter.Entities
{
    public class Hotel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string MapCoords { get; set; }

        public string Phones { get; set; }

        public int Rating { get; set; }

        public bool Parking { get; set; }

        public bool Pool { get; set; }

        public bool Sauna { get; set; }

        public bool TennisCourt { get; set; }

        public bool Gym { get; set; }

        public Hotel()
        {

        }
    }
}
