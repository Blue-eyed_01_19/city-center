﻿using System.Collections.Generic;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.Repositories.Abstract;
using System.Linq;
using CityCenter.WebUI.Abstract;

namespace CityCenter.BLL
{
    public class UsersManager : IUsersManager
    {
        #region Private Fields

        private readonly IUserRepository _userRepository;

        private readonly ISecurityManager _securityManager;

        #endregion

        #region Constructor

        public UsersManager(IUserRepository userRepository, ISecurityManager securityManager)
        {
            _userRepository = userRepository;
            _securityManager = securityManager;
        }

        #endregion

        #region IUsersManager

        public List<User> GetAllUsers()
        {
            return _userRepository.GetUsers();
        }

        public int GetUserIdByReservationId(int id)
        {
            return _userRepository.GetUserIdByReservationId(id);
        }

        public User GetUserById(int id)
        {
            return _userRepository.GetUserById(id);
        }

        public User GetUserByLogin(string login)
        {
            return  _userRepository.GetUserByLogin(login);
        }

        public User GetUserByEmail(string email)
        {
            return isEmailValid(email) ? _userRepository.GetUserByEmail(email) : null;
        }

        public User GetUserByPhone(string phone)
        {
            return isPhoneValid(phone) ? _userRepository.GetUserByPhone(phone) : null;
        }

        public string GetUserRegistrationToken(int userId)
        {
            return _userRepository.GetUserRegistrationToken(userId);
        }

        public string GetUserPasswordRecoveryToken(int userId)
        {
            return _userRepository.GetUserPasswordRecoveryToken(userId);
        }

        public void InsertUser(User user)
        {
            if (UserIsValid(user))
            {
                _securityManager.UpdateUserPassword(user);
                _userRepository.InsertUser(user);
            }
        }

        public void DeleteUser(User user)
        {
            if (user != null)
            {
                _userRepository.DeleteUser(user);
            }
        }

        public void UpdateUser(User user)
        {
            if (UserIsValid(user))
            {
                _userRepository.UpdateUser(user);
            }
        }

        public void UpdateUserPassword(User user)
        {
            if (isPasswordValid(user.Password))
            {
                _userRepository.UpdateUserPassword(user);
            }
        }

        public List<Role> GetRoles()
        {
            return _userRepository.GetRoles();
        }

        public List<Role> GetUserRoles(int userId)
        {
            return _userRepository.GetUserRoles(userId);
        }

        public List<User> GetUsersByRole(string roleName)
        {
            if (roleName == "All")
            {
                return _userRepository.GetUsers();
            }

            var role = _userRepository.GetRoleByName(roleName);
            return _userRepository.GetUsersByRole(role.Id);
        }

        public List<User> GetUsersByTextFilter(string text)
        {
            return text == "" ? _userRepository.GetUsers() : _userRepository.GetUsersByTextFilter(text);
        }

        public void SetRoles(int userId, bool isUser = false, bool isAdmin = false, bool isHotelAdmin = false, bool isCafeAdmin = false, bool isExcursionAdmin = false)
        {
            _userRepository.SetRoles(userId,
                               isAdmin: isAdmin,
                               isUser: isUser,
                               isHotelAdmin: isHotelAdmin,
                               isCafeAdmin: isCafeAdmin,
                               isExcursionAdmin: isExcursionAdmin);
        }

        public void UnBlockUser(int userId)
        {
            _userRepository.UpdateUserStatus(userId, true);
        }

        public void DeletePasswordRecoveryToken(int userId)
        {
            _userRepository.DeletePasswordRecoveryToken(userId);
        }

        public void UserConfirmRegistration(int userId)
        {
            SetRoles(userId, isUser: true);
            _userRepository.DeleteUnconfirmRegistration(userId);
            _userRepository.UpdateUserStatus(userId, true);
        }

        public void InsertUserFeedback(string login, string email, string message)
        {
            _userRepository.InsertUserFeedback(login, email, message);
        }
        #endregion

        #region Validators

        private bool UserIsValid(User user)
        {
            return (isNameValid(user.FirstName)) &&
                   (isNameValid(user.Surname)) &&
                   (isEmailValid(user.Email)) &&
                   (isPhoneValid(user.Phone));
        }

        private bool isPasswordValid(string pass)
        {
            return pass.Length > 0;
        }

        private bool isPhoneValid(string phone)
        {

            var result = phone.Length == 13;

            if (phone[0] != '+')
            {
                result = false;
            }

            for (var a = 1; a < phone.Length; a++)
            {
                if (!char.IsDigit(phone[a]))
                {
                    result = false;
                }
            }

            return result;
        }
        
        private bool isEmailValid(string mail)
        {
            var result = true;
            var atSplitted = mail.Split('@');

            if (atSplitted.Length != 2)
            {
                result = false;
            }

            else
            {
                var afterAt = atSplitted[1].Split('.');

                if (afterAt.Length != 2)
                {
                    result = false;
                }

                else
                {
                    if (afterAt[0].Length < 1 || afterAt[1].Length < 1)
                    {
                        result = false;
                    }

                    else
                    {
                        result = true;
                    }
                }
            }
            return result;
        }

        private bool isNameValid(string name)
        {
            const int maxLength = 30;
            var result = true;
            if (name.Length > maxLength || name.Length < 1)
            {
                result = false;
            }
            else
            {
                foreach (var c in name.Where(c => (!char.IsLetter(c)) &&
                                                   (c != '-') &&
                                                   (c != ' ')))
                {
                    result = false;
                }
            }
            return result;
        }

        #endregion
    }
}
