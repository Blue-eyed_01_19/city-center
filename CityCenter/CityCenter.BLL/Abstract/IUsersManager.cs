﻿using System.Collections.Generic;
using CityCenter.Entities;

namespace CityCenter.BLL.Abstract
{
    public interface IUsersManager
    {
        List<User> GetAllUsers();

        User GetUserById(int id);

        User GetUserByLogin(string login);

        User GetUserByEmail(string email);

        User GetUserByPhone(string phone);

        string GetUserRegistrationToken(int userId);

        string GetUserPasswordRecoveryToken(int userId);

        void InsertUser(User user);

        void DeleteUser(User user);

        void UpdateUser(User user);

        void UpdateUserPassword(User user);

        List<Role> GetRoles();

        List<Role> GetUserRoles(int userId);

        List<User> GetUsersByRole(string roleName);

        List<User> GetUsersByTextFilter(string text);

        void SetRoles(int userId, bool isUser = false, bool isAdmin = false, bool isHotelAdmin = false, bool isCafeAdmin = false, bool isExcursionAdmin = false);

        void UnBlockUser(int userId);

        void UserConfirmRegistration(int userId);

        void InsertUserFeedback(string login, string email, string message);

        int GetUserIdByReservationId(int id);

        void DeletePasswordRecoveryToken(int userId);
    }
}
