﻿using System.Threading.Tasks;
using CityCenter.Entities;

namespace CityCenter.BLL.Abstract
{
    public interface IEmailManager
    {
        void SendEmail(string mailTo, string subject, string body);

        void SendExcursionReservationNotification(User user, ExcursionReservationDetails reservation);

        void SendHotelReservationNotification(User user, HotelReservationDetails reservation);

    }
}
