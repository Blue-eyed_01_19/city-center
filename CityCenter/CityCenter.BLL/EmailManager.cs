﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using NLog;

namespace CityCenter.BLL
{
    public class EmailManager : IEmailManager
    {
        private readonly ILogger _logger;

        public EmailManager(ILogger logger)
        {
            _logger = logger;
        }

        public void SendEmail(string mailTo, string subject, string body)
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    using (var client = new SmtpClient())
                    {
                        var message = new MailMessage(new MailAddress("lviv.city.center@gmail.com", "Lviv City Center"), new MailAddress(mailTo));
                        message.To.Add(mailTo);
                        message.Subject = subject;
                        message.Body = body;
                        message.IsBodyHtml = true;
                        client.Send(message);
                        _logger.Info(" Message was sent to {0}. Subject : {1}", mailTo, subject);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                    throw;
                }
            });
        }

        public void SendExcursionReservationNotification(User user, ExcursionReservationDetails reservation)
        {
            var reservationStatus = "Declined";
            var mailSubject = "Excursion Reservation Notification";
            var mailBody = string.Format(
                "<div style=\"font-size:16px;\">Dear {0} {1}.<br/>"
                + "Your Reservation at hotel {2} was : {3}",
                user.FirstName, user.Surname, reservation.ExcursionName, reservationStatus);

            SendEmail(user.Email, mailSubject, mailBody);
        }

        public void SendHotelReservationNotification(User user, HotelReservationDetails reservation)
        {
            var reservationStatus = "Declined";
            var mailSubject = "Hotel Reservation Notification";
            var mailBody = string.Format(
                "<div style=\"font-size:16px;\">Dear {0} {1}.<br/>"
                + "Your Reservation at hotel {2} was : {3}",
                user.FirstName, user.Surname, reservation.HotelName, reservationStatus);

            SendEmail(user.Email, mailSubject, mailBody);
        }
    }
}
