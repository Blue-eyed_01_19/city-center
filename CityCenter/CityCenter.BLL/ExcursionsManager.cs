﻿using System;
using System.Collections.Generic;
using CityCenter.BLL.Abstract;
using CityCenter.Entities;
using CityCenter.Repositories.Abstract;

namespace CityCenter.BLL
{
    public class ExcursionsManager : IExcursionsManager
    {
        #region Private Fields

        private readonly IExcursionsRepository _excursionsRepository;

        #endregion

        #region Constructor

        public ExcursionsManager(IExcursionsRepository excursionsRepository)
        {
            _excursionsRepository = excursionsRepository;
        }

        #endregion

        #region IExcursionsManager implementation

        public int AddNewReservation(int userId, int excursionId, DateTime excursionDate)
        {
            return _excursionsRepository.InsertReservation(userId, excursionId, excursionDate, 0);
        }

        public List<Excursion> GetExcursions(int pageNumber, int pageSize)
        {
            return _excursionsRepository.GetExcursions(pageNumber, pageSize);
        }

        public List<ExcursionDescription> GetExcursionDescriptions()
        {
            return _excursionsRepository.GetExcursionDescriptions();
        }

        public List<ExcursionDescription> GetExcursionDescriptions(int pageNumber, int pageSize)
        {
            return _excursionsRepository.GetExcursionDescriptions(pageNumber, pageSize);
        }

        public int GetExcursionDescriptionsCount()
        {
            return _excursionsRepository.GetExcursionDescriptionsCount();
        }

        public Excursion GetExcursionById(int id)
        {
            return _excursionsRepository.GetExcursionById(id);
        }

        public Excursion GetExcursionByName(string name)
        {
            return _excursionsRepository.GetExcursionByName(name);
        }

        public ExcursionDescription GetExcursionDescriptionById(int excursionId)
        {
            return _excursionsRepository.GetExcursionDescriptionById(excursionId);
        }

        public int GetReservationDetailsCountByUserId(int userId)
        {
            return _excursionsRepository.GetReservationDetailsCountByUserId(userId);
        }

        public List<ExcursionReservationDetails> GetReservationDetailsByUserId(int userId, int pageNumber, int pageSize)
        {
            return _excursionsRepository.GetReservationDetailsByUserId(userId, pageNumber, pageSize);
        }

        public int GetAcceptedReservationsCountByExcursionId(int excursionId)
        {
            return _excursionsRepository.GetAcceptedReservationsCountByExcursionId(excursionId);
        }

        public List<ExcursionReservationDetails> GetAcceptedReservationsByExcursionId(int excursionId, int pageNumber, int pageSize)
        {
            return _excursionsRepository.GetAcceptedReservationsByExcursionId(excursionId, pageNumber, pageSize);
        }


        public int GetPendingReservationsCountByExcursionId(int excursionId)
        {
            return _excursionsRepository.GetPendingReservationsCountByExcursionId(excursionId);
        }

        public List<ExcursionReservationDetails> GetPendingReservationsByExcursionId(int excursionId, int pageNumber, int pageSize)
        {
            return _excursionsRepository.GetPendingReservationsByExcursionId(excursionId, pageNumber, pageSize);
        }

        public List<Excursion> GetExcursions()
        {
            return _excursionsRepository.GetExcursions();
        }

        public void UpdateExcursionImage(ExcursionDescription excursionDescription)
        {
            _excursionsRepository.UpdateExcursionImage(excursionDescription);
        }

        public void UpdateExcursionDescription(ExcursionDescription excursionDescription)
        {
            _excursionsRepository.UpdateExcursionDescription(excursionDescription);
        }

        public void UpdateExcursionName(string name, int id)
        {
            _excursionsRepository.UpdateExcursionName(name, id);
        }

        public void AcceptReservation(int reservationId)
        {
            _excursionsRepository.ChangeReservationStatus(reservationId, 2);
        }

        public void DeclineReservation(int reservationId)
        {
            _excursionsRepository.ChangeReservationStatus(reservationId, 1);
        }

        public List<Excursion> GetExcursionsByAdminId(int adminId)
        {
            return _excursionsRepository.GetExcursionsByAdminId(adminId);
        }

        public int AddNewExcursion(Excursion excursion, ExcursionDescription description, int userId)
        {
            excursion.BeginTime = new TimeSpan(excursion.BeginTime.Hours, excursion.BeginTime.Minutes, 0);
            var excursionId = _excursionsRepository.InsertExcursion(excursion);
            description.ExcursionId = excursionId;
            _excursionsRepository.InsertNewExcursionAdmin(excursionId, userId);
            _excursionsRepository.InsertExcursionDescription(description);
            return excursionId;
        }

        public List<ExcursionReservationDetails> GetTodayPendingReservations()
        {
            return _excursionsRepository.GetTodayPendingReservations();
        }

        #endregion
    }
}
