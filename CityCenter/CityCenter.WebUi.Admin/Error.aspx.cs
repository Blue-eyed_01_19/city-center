﻿using System;
using System.Web;

namespace CityCenter.WebUi.Admin
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                ErrorAnalysis();
            }
            catch
            { }
        }
        protected void btnBack_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/AdminPage.aspx");
        }

        private void ErrorAnalysis()
        {
            if (Server.GetLastError() == null)
            {
                lblErrorMessage.Text = "No information about an error";
                lblErrorReason.Text = "";
            }
            else
            {
                Exception ex = Server.GetLastError().GetBaseException();
                if (ex is HttpException)
                {
                    HttpException hex = ex as HttpException;
                    lblErrorMessage.Text = "Error: " + ex.Message;
                    lblErrorReason.Text = "";
                    if (hex.GetHttpCode() == 404)
                    {
                        lblErrorReason.Text = "If you typed that address in address bar, check whether the URL you specified is correct.";
                    }
                }
                else
                {
                    lblErrorMessage.Text = "Unexpected error: " + ex.Message;
                    lblErrorReason.Text = "";
                }
            }
        }
    }
}