﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CityCenter.Master" AutoEventWireup="true" CodeBehind="AdminPage.aspx.cs" Inherits="CityCenter.WebUI.Admin.Default" %>

<asp:Content ID="title" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="content" runat="server">
    <div class="wrapper-content">
        <div id="wrapper-search">
            <asp:TextBox ID="txtSearch" runat="server" CssClass="txt-search"></asp:TextBox>
            <asp:Button ID="btnReset" runat="server" Text="" OnClick="btnReset_Click" CssClass="btn-reset" />
            <asp:Button ID="btnSearch" runat="server" Text="" CssClass="btn-search" OnClick="btnSearch_Click" />
        </div>
        <div id="wrapper-button">
            <div id="add-button">
                <asp:Button ID="btnAdd" CssClass="control-button" runat="server" Text="Add" OnClick="btnAdd_Click" />
                <asp:Button ID="btnEdit" CssClass="control-button" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                <asp:Button ID="btnDelete" CssClass="control-button" runat="server" Text="Delete" OnClick="btnDelete_Click" />
            </div>
            <div id="editButton" runat="server" class="edit-button">
                <asp:Button ID="btnUpdate" CssClass="control-button" runat="server" Text="Update" OnClick="btnUpdate_Click" />
                <asp:Button ID="btnCancel" CssClass="control-button" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
            </div>
        </div>
        <div id="wrapper-grid">
            <div id="grid-view">
                <div>
                    <asp:ObjectDataSource ID="odsUsers" runat="server"
                        TypeName="CityCenter.BLL.UsersManager"
                        DataObjectTypeName="CityCenter.Entities.User"
                        SelectMethod="GetAllUsers"
                        InsertMethod="InsertUser"
                        DeleteMethod="DeleteUser"
                        UpdateMethod="UpdateUser"
                        OnObjectCreating="odsUsers_ObjectCreating"></asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="odsRoles" runat="server" ConflictDetection="CompareAllValues"
                        TypeName="CityCenter.BLL.UsersManager"
                        SelectMethod="GetRoles"
                        OnObjectCreating="odsUsers_ObjectCreating"></asp:ObjectDataSource>
                </div>
                <asp:DetailsView Visible="False" ID="dtvAddUser" runat="server" CssClass="add-request" AutoGenerateRows="False" DataSourceID="odsUsers" DefaultMode="Insert" AutoGenerateInsertButton="True" OnItemInserted="dtvAddUser_ItemInserted" OnModeChanged="dtvAddUser_ModeChanged">
                    <Fields>
                        <asp:TemplateField HeaderText="Login" SortExpression="Login">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Login") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="AddUser_txtLogin" runat="server" Text='<%# Bind("Login") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="AddUser_txtLogin" Display="Dynamic" SetFocusOnError="true"
                                    Text="can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Login") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Password" SortExpression="Password">
                            <InsertItemTemplate>
                                <asp:TextBox ID="AddUser_txtPass" runat="server" Text='<%# Bind("Password") %>' TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="AddUser_txtPass" Display="Dynamic" SetFocusOnError="true"
                                    Text="can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>
                            </InsertItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="AddUser_txtFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="AddUser_txtFirstName" Display="Dynamic" SetFocusOnError="true"
                                    Text="can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorFirstName" runat="server"
                                    ControlToValidate="AddUser_txtFirstName" Text="First name is invalid" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"
                                    ValidationExpression="^[a-zA-Zа-яА-ЯіІїЇ -]{1,15}$"></asp:RegularExpressionValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Surname" SortExpression="Surname">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="AddUser_txtSurname" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="AddUser_txtSurname" Display="Dynamic" SetFocusOnError="true"
                                    Text="can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorSurname" runat="server"
                                    ControlToValidate="AddUser_txtSurname" Text="Surname is invalid" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"
                                    ValidationExpression="^[a-zA-Zа-яА-ЯіІїЇ -]{1,15}$"></asp:RegularExpressionValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("Surname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="AddUser_txtEmail" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="AddUser_txtEmail" Display="Dynamic" SetFocusOnError="true"
                                    Text="can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server"
                                    ControlToValidate="AddUser_txtEmail" Text="Enter valid email" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                </asp:RegularExpressionValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="AddUser_txtPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="AddUser_txtPhone" Display="Dynamic" SetFocusOnError="true"
                                    Text="can not be empty" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" runat="server" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="AddUser_txtPhone" Text="Enter valid phone" ForeColor="Red"
                                    ValidationExpression="^\+[1-9]{1}[0-9]{11}$">
                                </asp:RegularExpressionValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>
                <asp:GridView ID="grvUsers" runat="server" DataSourceID="odsUsers" DataKeyNames="Id" AllowPaging="true" PageSize="20" OnPageIndexChanging="grvUsers_PageIndexChanging"
                    AutoGenerateColumns="False" Width="700px" OnSelectedIndexChanged="grvUsers_SelectedIndexChanged" OnRowUpdating="grvUsers_RowUpdating" OnSelectedIndexChanging="grvUsers_SelectedIndexChanging" CssClass="grid-view">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" />
                        <asp:TemplateField HeaderText="Id" SortExpression="Id">
                            <EditItemTemplate>
                                <asp:Label ID="lblEditId" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Login" SortExpression="Login">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditLogin" CssClass="edit-textbox" runat="server" Text='<%# Bind("Login") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEditLogin" runat="server" Text="can not be empty" ForeColor="Red" ControlToValidate="txtEditLogin" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Login") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditFirstName" CssClass="edit-textbox" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEditFirstName" runat="server" Text="can not be empty" ForeColor="Red" ControlToValidate="txtEditFirstName" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEditFirstName" runat="server"
                                    ControlToValidate="txtEditFirstName" Text="First name is invalid" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"
                                    ValidationExpression="^[a-zA-Zа-яА-ЯіІїЇ -]{1,15}$"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Surname" SortExpression="Surname">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditSurname" CssClass="edit-textbox" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEditSurame" runat="server" Text="can not be empty" ForeColor="Red" ControlToValidate="txtEditSurname" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEditSurname" runat="server"
                                    ControlToValidate="txtEditSurname" Text="Surname is invalid" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"
                                    ValidationExpression="^[a-zA-Zа-яА-ЯіІїЇ -]{1,15}$"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Bind("Surname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditEmail" CssClass="edit-textbox" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEditEmail" runat="server" Text="can not be empty" ForeColor="Red" ControlToValidate="txtEditEmail" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEditEmail" runat="server" Text="Invalid Email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEditEmail"></asp:RegularExpressionValidator>

                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEditPhone" CssClass="edit-textbox" runat="server" Text='<%# Bind("Phone") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEditPhone" runat="server" Text="can not be empty" ForeColor="Red" ControlToValidate="txtEditPhone" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEditPhone" runat="server" Text="Invalid Phone" ForeColor="Red" ValidationExpression="^\+[1-9]{1}[0-9]{11}$" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEditPhone"></asp:RegularExpressionValidator>

                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="Status">
                            <EditItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                    <EmptyDataTemplate>
                        <asp:Label ID="lblSearchResults" runat="server" Text="No records"></asp:Label>
                    </EmptyDataTemplate>
                    <EditRowStyle CssClass="edit-row-style"></EditRowStyle>
                    <RowStyle BackColor="White" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
                <asp:CheckBoxList ID="cblUserRoles" CssClass="role-table" runat="server" DataSourceID="odsRoles" DataTextField="Name" DataValueField="Id" Enabled="false">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
</asp:Content>
