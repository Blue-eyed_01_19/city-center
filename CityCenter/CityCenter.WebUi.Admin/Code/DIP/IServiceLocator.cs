﻿using System;

namespace CityCenter.WebUI.Admin.Code.DIP
{
    public interface IServiceLocator
    {
        T Resolve<T>();

        object Resolve(Type type);
    }
}