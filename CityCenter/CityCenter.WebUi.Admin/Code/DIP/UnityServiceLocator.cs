﻿using System;
using Microsoft.Practices.Unity;

namespace CityCenter.WebUI.Admin.Code.DIP
{
    public class UnityServiceLocator : IServiceLocator
    {
        #region Fields

        private readonly IUnityContainer _unityContainer;

        #endregion

        #region Constructors

        public UnityServiceLocator(IUnityContainer unityContainer)
        {
            _unityContainer = unityContainer;
        }

        #endregion

        #region IServiceLocator

        public object Resolve(Type type)
        {
            return _unityContainer.Resolve(type);
        }

        public T Resolve<T>()
        {
            return _unityContainer.Resolve<T>();
        }

        #endregion
    }
}