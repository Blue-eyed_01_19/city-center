﻿using System.Web;
using CityCenter.WebUI.Admin.Code.DIP;

namespace CityCenter.WebUI.Admin.Code
{
    public static class ApplicationExtensions
    {
        public static IServiceLocator GetServiceLocator(this HttpApplicationState application)
        {
            return (IServiceLocator) application["ServiceLocator"];
        }
    }
}