﻿using System;
using CityCenter.WebUI.Abstract;
using CityCenter.WebUI.Admin.Code;
using CityCenter.WebUI.Admin.Code.DIP;

namespace CityCenter.WebUI.Admin
{
    public partial class CityCenter : System.Web.UI.MasterPage
    {
        private ISecurityManager _securityManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            IServiceLocator serviceLocator = Application.GetServiceLocator();
            _securityManager = serviceLocator.Resolve<ISecurityManager>();
            loggedInUserName.Text = _securityManager.GetCurrentUser();
        }


        protected void signOut_OnClick(object sender, EventArgs e)
        {
            _securityManager.SignOut();
            Response.Redirect("~/Default.aspx");
        }
    }
}