﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CityCenter.BLL.Abstract;
using CityCenter.WebUI.Admin.Code;
using CityCenter.WebUI.Admin.Code.DIP;
using CityCenter.WebUI.Admin.Code.Keys;

namespace CityCenter.WebUI.Admin
{
    public partial class Default : Page
    {
        #region Fields

        private IUsersManager _usersManager;
        
        #endregion

        #region Event Handlers

        #region Initialization

        protected void Page_Load(object sender, EventArgs e)
        {
            IServiceLocator serviceLocator = Application.GetServiceLocator();
            _usersManager = serviceLocator.Resolve<IUsersManager>();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (Session[SessionKeys.BIND_METHOD] != null)
                {
                    odsUsers.SelectMethod = (string)Session[SessionKeys.BIND_METHOD];
                }
                grvUsers.DataBind();
            }
            catch (InvalidOperationException)
            {
                Session[SessionKeys.BIND_METHOD] = "GetAllUsers";
                odsUsers.SelectMethod = (string)Session[SessionKeys.BIND_METHOD];
                grvUsers.DataBind();
            }
        }

        protected void odsUsers_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = _usersManager;
        }

        #endregion

        #region Button events


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (NotInEditMode())
            {
                odsUsers.SelectParameters.Clear();
                Session[SessionKeys.BIND_METHOD] = "GetUsersByTextFilter";
                odsUsers.SelectParameters.Add("text", TypeCode.String, txtSearch.Text);

                UnselectRows();
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            if (NotInEditMode())
            {
                odsUsers.SelectParameters.Clear();
                Session[SessionKeys.BIND_METHOD] = "GetAllUsers";
                txtSearch.Text = "";
                UnselectRows();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (NotInEditMode())
            {
                dtvAddUser.Visible = true;
                UnselectRows();
                // handle (un)sucessful adding
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (NotInEditMode() && IsRowSelected())
            {
                grvUsers.DeleteRow(grvUsers.SelectedIndex);
                UnselectRows();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (NotInEditMode() && IsRowSelected())
            {
                grvUsers.EditIndex = grvUsers.SelectedIndex;
                btnEdit.Enabled = false;
                cblUserRoles.Enabled = true;
                ShowAdditionalButtons(true);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            grvUsers.UpdateRow(grvUsers.SelectedIndex, false);
            btnEdit.Enabled = true;
            UnselectRows();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            grvUsers.EditIndex = -1;
            btnEdit.Enabled = true;
            cblUserRoles.Enabled = false;
            ShowAdditionalButtons(false);
            UnselectRows();
        }

        #endregion

        #region grvUsers events

        protected void grvUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            cblUserRoles.Style.Add("display", "block");
            var roles = _usersManager.GetUserRoles(GetSelectedUserId());
            foreach (ListItem item in cblUserRoles.Items)
            {
                item.Selected = (roles.Find(x => x.Name == item.Text) != null);
            }
        }

        protected void grvUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            cblUserRoles.Enabled = false;
            SaveUserRoles();
            ShowAdditionalButtons(false);
        }

        protected void grvUsers_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            if (NotInEditMode())
            {

            }
            else
            {
                e.Cancel = true;
            }
        }

        protected void grvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (grvUsers.EditIndex != -1)
            {
                e.Cancel = true;
            }
        }
        #endregion

        #region Other events

        protected void dtvAddUser_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            dtvAddUser.Visible = false;
        }

        protected void dtvAddUser_ModeChanged(object sender, EventArgs e)
        {
            dtvAddUser.Visible = false;
        }

        #endregion

        #endregion

        #region Helpers

        private void SaveUserRoles()
        {
            bool isAdmin = cblUserRoles.Items.FindByText("Admin").Selected;
            bool isHotelAdmin = cblUserRoles.Items.FindByText("HotelAdmin").Selected;
            bool isCafeAdmin = cblUserRoles.Items.FindByText("CafeAdmin").Selected;
            bool isExcursionAdmin = cblUserRoles.Items.FindByText("ExcursionAdmin").Selected;
            bool isUser = cblUserRoles.Items.FindByText("User").Selected;
            _usersManager.SetRoles(GetSelectedUserId(),
                               isAdmin: isAdmin,
                               isUser: isUser,
                               isHotelAdmin: isHotelAdmin,
                               isCafeAdmin: isCafeAdmin,
                               isExcursionAdmin: isExcursionAdmin);
        }

        private int GetSelectedUserId()
        {
            var label = (Label)grvUsers.SelectedRow.FindControl("lblId") ?? (Label)grvUsers.SelectedRow.FindControl("lblEditId");
            return int.Parse(label.Text);
        }

        private void ShowAdditionalButtons(bool show)
        {
            editButton.Style.Add("display", show ? "block" : "none");
        }

        private void UnselectRows()
        {
            grvUsers.SelectedIndex = -1;
            cblUserRoles.Style.Add("display", "none");
        }

        private bool NotInEditMode()
        {
            var inEditMode = false;
            if (grvUsers.EditIndex != -1)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Not allowed", "<script>alert('Not allowed while editing');</script>", false);
                inEditMode = true;
            }
            return !inEditMode;
        }

        private bool IsRowSelected()
        {
            var isRowSelected = false;
            if (grvUsers.SelectedIndex != -1)
            {
                isRowSelected = true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Not allowed", "<script>alert('Select row first');</script>", false);
            }
            return isRowSelected;
        }

        #endregion

    }
}