﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CityCenter.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="CityCenter.WebUi.Admin.Error" %>

<asp:Content ContentPlaceHolderID="title" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="wrapper-content">
        <div id="wrapper-error">
            <div class="error">
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error-message" Text="Error Message"></asp:Label>
            </div>
            <div class="error">
                <asp:Label ID="lblErrorReason" runat="server" CssClass="error-reason" Text="Error Reason"></asp:Label>
            </div>
            <div style="text-align: center">
                <asp:Button ID="btnBack" runat="server" Text="" OnClick="btnBack_OnClick" CssClass="btn-back" />
            </div>
        </div>
    </div>
</asp:Content>
